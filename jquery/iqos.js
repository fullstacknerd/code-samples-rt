/**
 * Скрипт для лендинга Айкос
 */


/* eslint-disable */
/* global jQuery */

'use strict';

$(document).ready(function() {
    
    var iqos = {
        modals: {
            $sms_popup: $('.js-modal-sms'),
            $user_popup: $('.js-modal-user'),
            $success_popup: $('.js-modal-thankyou'),
            $loader: $('.js-page-preloader'),
            $rules_popup: $('.js-rules-modal'),
        },
        code: '',
        data: {
            firstName: '',
            lastName: '',
            birthDate: '',
            color: 0,
            model: 0,
            phone: '',
            phone_raw: '',
        },
        
        init: function() {    
            this.maskIqosFormPhone();
            this.watchIqosForm();
            
            // $('.js-color-menu').on('click', this.showColorSelect.bind(this));
            $('.js-show-rules').on('click', this.showRules.bind(this));
        },
        
        smsModal: function() {
            
            var $modal = this.modals.$sms_popup;    
            var $input = $('.js-sms-input');
            var $submit = $('.js-sms-submit');
            var $newcode = $('.js-sms-resend');
            var $reply = $('.js-sms-reply');
            var $error = $('.js-sms-error');
            var $number = $('.js-sms-number');
            var $close = $('.js-sms-close');
            
            if ($error.hasClass('m-show')) {
                $reply.removeClass('m-show');
            }
            
            $submit.prop('disabled', true);
            $modal.addClass('m-show');
            clearListeners();
            
            $number.text(this.data.phone_raw);
            
            $input.inputmask({"mask": "9999"});
            $input.on('keyup', inputChange.bind(this));
            
            // $newcode.on('click', this.smsResend.bind(this));
            
            $newcode.on('click', (function(evt){
                evt.preventDefault();
                $error.removeClass('m-show')
                $reply.addClass('m-show');
                this.smsResend();
            }).bind(this));
            
            $close.on('click', function(evt){
                $modal.removeClass('m-show');
                clearListeners();
            });
            
            $submit.on('click', (function(evt){
                evt.preventDefault();
                $modal.removeClass('m-show');
                clearListeners();
                this.smsSend();
            }).bind(this));
            
            
            function inputChange(evt){
                
                var raw_sms = $input.val();
                var sms = clearSms(raw_sms);
                
                if (sms.length === 4) {
                    this.code = sms;
                    $submit.prop('disabled', false);
                    // $submit.on('click', this.smsSend.bind(this));
                } else {
                    $submit.prop('disabled', true);
                }
            }
            
            function clearListeners(){
                $close.off('click');
                $newcode.off('click');
                $input.off('keyup');
                $submit.off('click');
                $input.val('');
            }
            
            function clearSms(str) {
                return str.replace(/[+-\s{()}_]/g, '');
            }
        },
        
        smsSend: function(){
            
            this.showLoader();
            
            var url = '/ajax/iqos/phone/';
            var data = {};
            
            data.code = this.code;
            data.phone = this.data.phone;
            
            $.ajax({
                url: url,
                method: 'POST',
                data: data
            })
            .done((function() {
                this.hideLoader();
                this.userModal();
            }).bind(this))
            .fail((function(jqXHR) {
                if (jqXHR.status === 400) {
                    this.hideLoader();
                    this.smsError(jqXHR);
                } else if (jqXHR.status === 418) {
                    this.hideLoader();
                    this.showIsRegisteredModal();
                } else {
                    console.log('Request error: ', jqXHR.status);
                }
            }).bind(this))
        },
        
        smsError: function(xhr) {
            
            var error_text = xhr.responseJSON.error;
            
            var $error = $('.js-sms-error');
            var $reply = $('.js-sms-reply');
            
            if ($reply.hasClass('m-show')) {
                $reply.removeClass('m-show');
            }
            
            $error.text(error_text);
            $error.addClass('m-show');
            this.smsModal();
        },
        
        smsResend: function(evt) {
            
            var url = '/ajax/iqos/phone/?phone=';
            url += this.data.phone;
            
            $.get(url, function(data) {
                console.log( "New code sent" );
            }).fail(function() {
                console.log( "Request error" );
            });
        },
        
        smsClearErrors: function() {
            var $error = $('.js-sms-error');
            var $reply = $('.js-sms-reply');
            
            $error.removeClass('m-show');
            $reply.removeClass('m-show');
        },
        
        userModal: function() {
            
            var $modal = this.modals.$user_popup;
            
            var $firstname = $modal.find('.js-user-firstname'),
            $lastname = $modal.find('.js-user-lastname'),
            $birthdate = $modal.find('.js-user-birthdate'),
            $birthdate_error = $modal.find('.js-birthdate-error'),
            $submit = $modal.find('.js-user-submit'),
            $close = $modal.find('.js-user-close');
            
            $modal.addClass('m-show');
            
            $birthdate.inputmask({'mask': '99/99/9999'});
            
            $close.on('click', (function(evt) {
                evt.preventDefault();
                $modal.removeClass('m-show');
                clearListeners();
            }).bind(this));
            
            $submit.on('click', (function(evt) {
                evt.preventDefault();
                $modal.removeClass('m-show');
                clearListeners();
                this.userFormSend();
            }).bind(this));
            
            $firstname.on('keyup', validateUserData.bind(this));
            $lastname.on('keyup', validateUserData.bind(this));
            $birthdate.on('keyup', validateUserData.bind(this));
            
            $firstname.on('blur', validateFNLength.bind(this));
            $lastname.on('blur', validateLNLength.bind(this));
            
            function clearListeners() {
                $submit.off('click');
                $firstname.off('keyup');
                $lastname.off('keyup');
                $birthdate.off('keyup');
                $firstname.off('blur');
                $lastname.off('blur');
            }
            
            function validateFNLength() {
                var firstname = $firstname.val();
                
                if (firstname.length < 2) {
                    $firstname.addClass('m-error');
                }
            }
            
            function validateLNLength() {
                var lastname = $lastname.val();
                
                if (lastname.length < 2) {
                    $lastname.addClass('m-error');
                }
            }
            
            function validateUserData() {
                var ready = true;
                
                var firstname = $firstname.val();
                var lastname = $lastname.val();
                var birthdate = $birthdate.val();
                var bd_cleared = clearBirthdate(birthdate);
                
                
                if (firstname.length < 2) {
                    ready = false;
                } else {
                    $firstname.removeClass('m-error');
                }
                
                if (lastname.length < 2) {
                    ready = false;
                } else {
                    $lastname.removeClass('m-error');
                }
                
                if (bd_cleared.length < 8) {
                    ready = false;
                    $birthdate.removeClass('m-error');
                } else {
                    var dateIsCorrect = checkDate(birthdate);
                    var ageIsUnder = checkAge(birthdate);
                    
                    if (!dateIsCorrect) {
                        ready = false;
                        $birthdate_error.html('Поле заполнено </br>некорректно');
                        $birthdate.addClass('m-error');
                    } else if (ageIsUnder) {
                        ready = false;
                        $birthdate_error.html('В акции могут <br> участвовать только <br>совершеннолетние <br> покупатели');
                        $birthdate.addClass('m-error');
                    } else {
                        $birthdate.removeClass('m-error');
                    }
                }
                
                if (ready === true) {
                    
                    this.data.firstName = firstname;
                    this.data.lastName = lastname;
                    this.data.birthDate = birthdate;
                    
                    $submit.prop('disabled', false);
                } else {
                    $submit.prop('disabled', true);
                }
            }
            
            function checkAge(date) {
                var tooYoung = false;
                
                // parse user birthdate
                var arrDate = date.split('/');
                var userDay = parseInt(arrDate[0], 10);
                var userMonth = parseInt(arrDate[1], 10);
                var userYear = parseInt(arrDate[2], 10);
                
                // parse current date
                var today = new Date();
                var nowDay = today.getDate();
                var nowMonth = today.getMonth()+1; //January is 0!
                var nowYear = today.getFullYear();
                
                // check difference
                if ((nowYear - userYear) < 18) {
                    tooYoung = true;
                } else if ((nowYear - userYear) === 18) {
                    if ((nowMonth - userMonth) < 0) {
                        tooYoung = true;
                    } else if ((nowDay - userDay) < 0) {
                        tooYoung = true;
                    }
                }
                return tooYoung;
            }
            
            function checkDate(date) {
                var correct = true;
                var arrDate = date.split('/');
                var day = parseInt(arrDate[0], 10);
                var month = parseInt(arrDate[1], 10);
                var year = parseInt(arrDate[2], 10);
                
                if (day <= 0 || day > 31) {
                    correct = false;
                }
                if (month <= 0 || month > 12) {
                    correct = false;
                }
                if (year <= 1910 || year > 2018) {
                    correct = false;
                }
                
                return correct;
            }
            
            function clearBirthdate(str) {
                return str.replace(/[+-\s{()}_\/]/g, '');
            }
        },
        
        userFormSend: function() {
            
            var url = '/ajax/iqos/user/';
            
            var data = {
                firstName: this.data.firstName,
                lastName:  this.data.lastName,
                birthDate: this.data.birthDate,
                model: this.data.model,
                color: this.data.color,
                // color: window.IQOS_color_id,
                phone: this.data.phone
            };
            
            this.showLoader();
            
            $.ajax({
                url: url,
                method: 'POST',
                data: data
            })
            .done((function(data) {
                this.hideLoader();
                this.addToCart(data.code);
                this.successModal(data);
            }).bind(this))
            .fail((function(jqXHR) {
                if (jqXHR.status === 400) {
                    this.hideLoader();
                    this.showIsRegisteredModal();
                }
            }).bind(this));
        },
        
        successModal: function(data) {
            
            var $modal = this.modals.$success_popup;
            var $link = $modal.find('.b-iqos-thankyou__link');
            var $name = $modal.find('.js-product-name');
            var $close = $('.js-thankyou-close');
            
            $link.attr('href', data.url);
            $name.text(data.name)
            $modal.addClass('m-show');
            $close.on('click', function(evt){
                $close.off('click');
                $modal.removeClass('m-show');
            });
        },
        
        addToCart: function(id) {
            
            var url = '/cart/updateQuantity';
            
            var data = {
                productCodePost: id,
                qty: 1
            };
            
            $.ajax({
                url: url,
                method: 'POST',
                data: data
            }).done((function(data) {
                console.log('Succesfully added to cart');
            }).bind(this))
            .fail(function() {
                console.log('Request failed');
            })
        },
        
        watchIqosForm: function() {
            var $input = $('.js-color-input ,.js-phone, .js-agree');
            $input.on('change', this.checkIqosForm.bind(this));
        },
        
        submitIqosForm: function(evt) {
            evt.preventDefault();
            
            this.showLoader();
            
            var url = '/ajax/iqos/phone/?phone=';
            url += this.data.phone;
            
            $.get(url)
            .done((function() {
                this.hideLoader();
                this.smsClearErrors();
                this.smsModal();
            }).bind(this))
            .fail(function() {
                console.log( "Request error" );
            });      
        },
        
        checkIqosForm: function(evt) {
            
            var ready = true;
            
            var $form = $(evt.target).closest('.js-form');
            var $color = $form.find('.js-color-input');
            var $model = $form.find('.js-model-input');
            var $phone = $form.find('.js-phone');
            var $agree = $form.find('.js-agree');
            var $submit = $form.find('.js-form-submit');
            
            var phone_raw = $phone.val();
            var phone_num = clearPhone(phone_raw);
            
            $submit.off('click');
            $submit.on('click', this.submitIqosForm.bind(this));
            
            if ($color.val().length === 0) {
                ready = false;
            }
            
            if ($agree && $agree.is(':not(:checked)')) {
                ready = false;
            }
            
            if (phone_num.length < 11) {
                ready = false;
            }
            
            if (ready === true) {
                var color = $color.val();
                var model = $model.val();
                this.data.color = color;
                this.data.model = model;
                this.data.phone = phone_num;
                this.data.phone_raw = phone_raw;
                
                $submit.prop('disabled', false)
                
            } else {
                $submit.prop('disabled', true);
            }
            
            function clearPhone(str) {
                return str.replace(/[+-\s{()}_]/g, '');
            }
        },
        
        maskIqosFormPhone: function(){
            // mask +7 (495) 504-34-78  
            $('.js-phone').inputmask({"mask": "+7 (999) 999-99-99"});
        },
        
        showLoader: function() {
            this.modals.$loader.addClass('m-show');
        },
        
        hideLoader: function() {
            this.modals.$loader.removeClass('m-show');
        },
        
        showRules: function(evt){
            evt.preventDefault();
            
            var $modal = this.modals.$rules_popup;
            var $close = $('.js-rules-close');
            
            $modal.addClass('m-show');
            
            $close.on('click', (function(evt){
                $modal.removeClass('m-show');
                $close.off('click');
            }).bind(this))
        },
        
    };
    
    iqos.init();
});
