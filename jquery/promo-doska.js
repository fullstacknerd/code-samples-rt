/**
 * Скирпт промо страницы "Доски"
 */

'use strict';

document.addEventListener('DOMContentLoaded', function () {

    var board = {
        controls: {
            $city_input:      $('.js-city-input'),
            $city_modal:      $('.js-city-modal'),
            $load_modal:      $('.js-page-preloader'),
            $user_modal:      $('.js-user-modal'),
            $success_modal:   $('.js-success-modal'),
            $success_city :   $('.js-success-city'),
            $error_modal:     $('.js-error-modal'),
            $rules_modal:     $('.js-rules-modal'),
            $allup_modal:     $('.js-allup-modal'),
            $test_section:    $('.js-board-test'),
            $test_wrapper:    $('.js-test-wrapper'),
            $choose_section:  $('.js-section-choose'),
            $yours_section:   $('.js-section-yours'),
            $btn_go_to_test:  $('.js-go-to-test'),
            $btn_test_repeat: $('.js-test-repeat'),
            $btn_get_board:   $('.js-get-board'),
            $btn_get_notest:  $('.js-get-notest')
        },

        vars: {
            end: [],
            city: {},
            test: {},
            result: {},
            boards: {},
            context: {},
            content: 'test'
        },
        
        init: function() {
          this.showPreloader();
          this.getEndData();
          this.getCity();
        },

        getCity: function() {

            $.get({
                url: '/ajax/city/set'
            }).done(
                function(data) {
                    board.selectCity(data);
            }).fail(
                function(jqXHR){
                    console.log( 'Request failed with status ' + jqXHR.status + ': ' + jqXHR.statusText );
            });
        },
        
        getEndData: function() {

            $.get({
                url: '/ajax/lb/ending'
            }).done(
                (function(data) {
                    this.vars.end = data;
            }).bind(this)).fail(
                function(jqXHR){
                    console.log( 'Request failed with status ' + jqXHR.status + ': ' + jqXHR.statusText );
            });
        },
        
        renderEndData: function(city) {
                    
          var end_data = this.vars.end;
          var user_city = city.toUpperCase();
        
          var msg_all = '';
          var msg_not_all = '';
          var message = '';
          
          if (end_data.length !== 0) {
            for (var i = 0; i < end_data.length; i++) {
              var data_item = end_data[i];
              
              if (data_item.city === user_city) {
                data_item.code === 'all' ? msg_all = data_item.text : msg_not_all = data_item.text;
              }
            }
          }
          
          if (msg_all || msg_not_all) {
            msg_all.length !== 0 ? message = msg_all : message = msg_not_all;
          }
          
          if (message.length !== 0) {
            $('.js-allup-text').html(message);
            board.controls.$allup_modal.addClass('m-show');
            board.controls.$load_modal.removeClass('m-show');
            
            // var $close = $('.js-allup-close');
            // $close.on('click', (function () {
            //   this.controls.$allup_modal.removeClass('m-show');
            //   $close.off('click');
            // }).bind(this));
          } else {
            board.controls.$load_modal.removeClass('m-show');
          }
        },

        selectCity(data) {

            /**
             * Подставляет значения в меню выбора города в зависимости от данных
             * полученных с геолокатора
             *
             * @param {obj} {city_id: msk/spb}
            */

            var city = data.city_id;

            board.vars.city.id = city;

            var $city_suggestion   = $('.js-city-detected'),
                $city_confirm_btn  = $('.js-city-correct'),
                $city_switch_text  = $('.js-city-change-text'),
                $city_switch_btn   = $('.js-city-change');

            switch(city) {
                case 'spb':
                     $city_suggestion.html( 'Ваш город – Санкт-Петербург?' );
                     $city_switch_text.html( 'Нет, Москва' );
                     $city_confirm_btn.data('city-name', 'spb');
                     $city_switch_btn.data('city-name', 'msk');
                     break;

                case 'msk':
                    $city_suggestion.html( 'Ваш город – Москва?' );
                    $city_switch_text.html( 'Нет, Санкт-Петербург' );
                    $city_confirm_btn.data('city-name', 'msk');
                    $city_switch_btn.data('city-name', 'spb');
                    break;

                default:
                    $city_suggestion.html( 'Ваш город – Москва?' );
                    $city_switch_text.html( 'Нет, Санкт-Петербург' );
                    $city_confirm_btn.data('city-name', 'msk');
                    $city_switch_btn.data('city-name', 'spb');
                    break;
            }

            board.controls.$city_modal.addClass('m-show');
            board.positionModal(board.controls.$city_modal);

            board.controls.$load_modal.removeClass('m-show');

            board.controls.$city_input.click(function(evt) {
                // Защита от прокликивания
                board.controls.$city_input.off();

                board.vars.city.id = $(evt.currentTarget).data('city-name');
                board.vars.city.id === 'msk' ? board.controls.$success_city.html('av.ru') : board.controls.$success_city.html('spb.av.ru');

                board.dataLayerEvent('Interactions', 'switch', 'cityName', null, board.vars.city.id, null);
                board.setCity( board.vars.city.id );
            });
        },

        setCity: function(city) {
          // this.renderEndData(city);
          /**
           * Отправляет выбранный пользователем город и
           * вызывает метод загрузки теста
           *
           * @param {str} city - msk / spb
          */

            $.ajax({
                url: '/ajax/city/set',
                data: 'city_id=' + city,
                method: 'POST'
            }).done(
                function() {
                    board.preparePage();
            }).fail(
                function(jqXHR){
                    console.log( 'Request failed with status ' + jqXHR.status + ': ' + jqXHR.statusText );
            });
        },

        preparePage: function() {

          /**
           * Подготовка страницы: получение вопросов и
           * вариантов ответа теста через get запрос.
           * После успешного получения данных,
           * данные сохраняются в board.vars.test и
           * вызывается метод renderTest()
           *
          */

            board.controls.$city_modal.removeClass('m-show');
            board.showPreloader();

            $.get({
                url: '/ajax/lb/questions'
            }).done(
                function(data) {
                    board.vars.test = data;
                    board.renderTest();
            }).fail(
                function(jqXHR){
                    console.log( 'Request failed with status ' + jqXHR.status + ': ' + jqXHR.statusText );
            });

            $('.js-copy-link').click(board.showRules);
        },

        renderTest: function() {

            /**
             *  Метод шаблонизирует и выводит вопросы теста с вариантами ответов
             *
             */

            var test_form_template =
                '<form class="b-board-test__form js-test-form">' +
                  '<input type="radio" class="b-board-test__toggler" id="show-quest-1" data-quest-id="1" name="test-toggle" checked>' +
                  '<input type="radio" class="b-board-test__toggler" id="show-quest-2" data-quest-id="2" name="test-toggle" disabled>' +
                  '<input type="radio" class="b-board-test__toggler" id="show-quest-3" data-quest-id="3" name="test-toggle" disabled>' +
                  '<input type="radio" class="b-board-test__toggler" id="show-quest-4" data-quest-id="4" name="test-toggle" disabled>' +
                '</form>';

            $(test_form_template).appendTo( board.controls.$test_wrapper );

            for (var key in board.vars.test) {

                var idx       = parseInt(key) + 1,
                    entry     = board.vars.test[key],
                    question  = entry.question,
                    answers   = entry.answers,
                    id        = entry.id;


                var test_field_template =
                    '<fieldset class="b-board-test__field" data-quest-id="' + idx + '">' +
                      '<div class="b-board-test__block">' +
                        '<legend class="b-board-test__question">' + question + '</legend>' +
                          '<ul class="b-board-test__answers js-test-answers" data-quest-id="' + idx + '">' +
                          '</ul>' +
                          '<p class="b-board-test__warning js-test-warning">Чтобы перейти к следующему вопросу, выберите вариант ответа</p>' +
                          '<label for="show-quest-' + (idx + 1) + '" class="b-board-test__toggler-label js-test-toggler">' +
                            '<span>Дальше</span>' +
                          '</label>' +
                        '</div>' +
                      '</fieldset>';

                var test_field_last_template =
                    '<fieldset class="b-board-test__field" data-quest-id="' + idx + '" data-id="">' +
                      '<div class="b-board-test__block">' +
                        '<legend class="b-board-test__question">' + question + '</legend>' +
                          '<ul class="b-board-test__answers js-test-answers" data-quest-id="' + idx + '">' +
                          '</ul>' +
                          '<p class="b-board-test__warning js-test-warning">Чтобы перейти к следующему вопросу, выберите вариант ответа</p>' +
                          '<button class="b-board-test__button b-board-test__submit js-test-submit">' +
                            '<span>Дальше</span>' +
                          '</button>' +
                      '</fieldset>';

                if (idx < 4) {
                    $(test_field_template).appendTo( $('.js-test-form') );
                } else {
                    $(test_field_last_template).appendTo( $('.js-test-form') );
                }


                $(answers).each(function(i, answer){
                    var id   = answer.id,
                        text = answer.answer;

                    var $block = $('.js-test-answers[data-quest-id="' + idx + '"]');

                    var answer_template =
                        '<li class="b-board-test__answer">' +
                            '<input type="radio" class="b-board-test__input" id="' + id + '" value="' + id + '" name="quest-' + idx + '">' +
                            '<label for="' + id + '" class="b-board-test__label">' +
                                text +
                            '</label>' +
                        '</li>';


                    $(answer_template).appendTo($block);
                });
            }

            // board.controls.$test_section.addClass('m-show');
            // board.controls.$load_modal.removeClass('m-show');
            
            this.renderEndData(this.vars.city.id);
            board.controls.$test_section.addClass('m-show');

            board.test();
        },

        test: function() {

            var $no_test     = $('.js-no-test'),
                $test_submit = $('.js-test-submit');

            watch();

            $('.b-board-test__toggler').change( watch );
            $('.js-test-toggler').click( function(evt){
                if ( checkIfSelected(evt) ) {
                    var context = window.pageContext ? window.pageContext : 'client';
                    var $block = $(evt.currentTarget).closest('.b-board-test__field');
                    var pos = $block.data('quest-id');
                    var answer_id = $block.find('.b-board-test__input:checked').val();
                    var answer_text = $block.find('.b-board-test__label[for="' + answer_id + '"]').html();
                    var content;

                    board.dataLayerEvent('Interactions', 'click', 'nextQuestion', context, answer_text, pos);
                }
            });

            function checkIfSelected(evt) {
                var source = evt.currentTarget || evt.target;
                var $fieldset = $( source ).closest('fieldset.b-board-test__field');
                var $warning  = $fieldset.find('.js-test-warning'),
                    selected  = $fieldset.find('.b-board-test__input:checked');

                  if (selected.length === 0) {
                      $warning.addClass('m-show');
                      return false;
                  } else {
                      $(source).off();
                      return true;
                  }
            }

            function watch() {

                var $toggler  = $('.b-board-test__toggler:checked'),
                    $toggl_id = $( $toggler ).data('quest-id'),
                    $block    = $('.b-board-test__field[data-quest-id="' + $toggl_id + '"]'),
                    $answers  = $( $block ).find('.b-board-test__input'),
                    $warning  = $( $block ).find('.js-test-warning');

                $answers.change(function(){
                    if ( $warning.hasClass('m-show') ) {
                         $warning.removeClass('m-show');
                    }

                    $('#show-quest-' + ($toggl_id + 1) ).prop('disabled', false);
                    $answers.off();
                });
            }


            function clearListeners() {
                $('.b-board-test__toggler').off();
                $('.js-test-toggler').off();
                $no_test.off();
                $test_submit.off();
            };

            $no_test.click( function(evt){
                evt.preventDefault();
                clearListeners();
                board.vars.content = 'manual';
                board.dataLayerEvent('Interactions', 'click', 'chooseBoard', null, null, null);
                board.skipTest();
            });

            $test_submit.click(function(evt){
                evt.preventDefault();

                if ( checkIfSelected(evt) ) {
                    clearListeners();

                    var context = window.pageContext ? window.pageContext : 'client';
                    var $block = $(evt.currentTarget).closest('.b-board-test__field');
                    var pos = $block.data('quest-id');
                    var answer_id = $block.find('.b-board-test__input:checked').val();
                    var answer_text = $block.find('.b-board-test__label[for="' + answer_id + '"]').html();
                    var content;

                    board.dataLayerEvent('Interactions', 'click', 'nextQuestion', context, answer_text, pos);
                    board.submitTest();
                }
            });
        },

        submitTest: function(evt) {

            board.showPreloader();

            var $test_input = $('.b-board-test__input:checked');
            var formData = '';

            $test_input.each(function(index, item){
                var name  = 'question-' + (index + 1),
                    value = $(item).attr('value');

                formData += name + '=' + value;

                if (index < $test_input.length - 1) {
                  formData += '&'
                }
            });

            $.ajax({
                url: '/ajax/lb/results',
                data: formData,
                contentType: 'application/x-www-form-urlencoded',
                processData: false,
                method: 'POST'
            }).done(
                function(data) {
                  board.vars.result = data;
                  board.renderResult();
            }).fail(
                function(jqXHR){
                    console.log( 'Request failed with status ' + jqXHR.status + ': ' + jqXHR.statusText );
            });
        },

        skipTest: function(evt) {

            // evt.preventDefault();
            board.showPreloader();

            if ( jQuery.isEmptyObject( board.vars.boards ) ) {

                $.get({
                    url: '/ajax/lb/results_all'
                }).done(
                    function(data) {
                        board.vars.boards = data;
                        board.renderAllBoards();
                }).fail(
                    function(jqXHR){
                        console.log( 'Request failed with status ' + jqXHR.status + ': ' + jqXHR.statusText );
                });
            } else {
                board.controls.$test_section.removeClass('m-show');
                board.controls.$load_modal.removeClass('m-show');
                board.controls.$choose_section.addClass('m-show');
            }

            // Получить доску без теста
            board.controls.$btn_get_notest.click(board.getBoard)
        },

        renderResult: function() {

            /**
             *  Метод подставляет изображение в результат
             * теста (значение берется из board.vars.result)
             *
             */

            var board_name  = board.vars.result.name,
                board_img   = board.vars.result.image_url;

            board.vars.context.name = board.vars.result.name;
            board.vars.context.sku = board.vars.result.sku;

            var $your_board = $('.js-yours-board');

            var your_board_template;

            if (board_img != null && board_name != null) {
                your_board_template = '<img src="' + board_img + '" width="350" height="350" alt="' + board_name + '">';
            } else {
                your_board_template = '<img src="/static/img/doska/1.jpg" width="350" height="350" alt="Разделочная доска Маэстро высокой кухни">';
            }

            $your_board.empty();
            $your_board.append(your_board_template);

            board.controls.$test_section.removeClass('m-show');
            board.controls.$choose_section.removeClass('m-show');
            board.controls.$load_modal.removeClass('m-show');
            board.controls.$yours_section.addClass('m-show');

            board.controls.$btn_test_repeat.click(
                function(evt){
                    evt.preventDefault();
                    board.dataLayerEvent('Interactions', 'click', 'anotherTest', board.vars.context.name, null, null);
                    board.repeatTest();
            });


            board.controls.$btn_get_board.click( board.getBoard );
        },

        renderAllBoards: function() {

            for (var key in board.vars.boards) {

                var idx   = parseInt(key) + 1,
                    item  = board.vars.boards[key],
                    name  = item.name,
                    alias = item.short_name,
                    sku   = item.sku,
                    img   = item.image_url;

                var $choose_list = $('.js-boards-list');

                var board_template =
                    '<li class="b-board-choose__board">' +
                      '<label for="board-' + idx + '" class="b-board-choose__label">' +
                        '<input type="radio" class="b-board-choose__input js-choose-input" id="board-' + idx + '" name="choose-board" value=' + sku + ' data-board-name="' + name + '">' +
                        '<div class="b-board-choose__img-wrapper">' +
                          '<img src="' + img + '" alt="' + alias + '" class="b-board-choose__image">' +
                        '</div>' +
                        '<span class="b-board-choose__board-name">' + alias + '</span>' +
                      '</label>' +
                    '</li>';

                $(board_template).appendTo($choose_list);
            }

            $choose_list.find('input#board-1').prop('checked', true);

            board.vars.context.name = $('input#board-1').data('board-name');
            board.vars.context.sku = $('input#board-1').val();

            $('.js-choose-input').change(function(evt){
                var source = evt.currentTarget;
                board.vars.context.name = $(source).data('board-name');
                board.vars.context.sku = $(source).val();
            });

            board.controls.$test_section.removeClass('m-show');
            board.controls.$yours_section.removeClass('m-show');
            board.controls.$load_modal.removeClass('m-show');
            board.controls.$choose_section.addClass('m-show');

            board.controls.$btn_go_to_test.click( function(evt){
                evt.preventDefault();

                board.vars.content = 'test';
                board.dataLayerEvent('interactions', 'click', 'goToTest', null, null, null);
                board.repeatTest();
            });
        },

        repeatTest: function(evt) {

            $('.b-board-test__toggler').each(function(idx, toggler){
                if (idx === 0) {
                  $(toggler).prop('checked', true);
                  $(toggler).prop('disabled', false);
                } else {
                  $(toggler).prop('checked', false);
                  $(toggler).prop('disabled', true);
                }
            });

            $('.b-board-test__input').prop('checked', false);

            board.controls.$load_modal.removeClass('m-show');

            board.test();

            board.controls.$choose_section.removeClass('m-show');
            board.controls.$yours_section.removeClass('m-show');
            board.controls.$load_modal.removeClass('m-show');
            board.controls.$test_section.addClass('m-show');
        },

        getBoard: function(evt) {
            evt.preventDefault();

            var source = source = $(evt.currentTarget).data('source');

            board.dataLayerEvent('Interactions', 'click', 'getGiftBoard', board.vars.context.name, board.vars.content, null);

            var input_states = {
                  name: false,
                  email: false,
                  phone: true,
                  rules: true
            }

            var $userform_input  = $('.js-input-userinfo'),
                $name_input      = $('#user-name'),
                $email_input     = $('#user-email'),
                $email_error     = $('.js-email-error'),
                $phone_input     = $('#user-phone'),
                $phone_error     = $('.js-phone-error'),
                $rules_input     = $('.js-userform-rules'),
                $rules_link      = $('.js-rules-link'),
                $rules_close     = $('.js-rules-close'),
                $userform_submit = $('.js-userform-submit'),
                $userform_pseudo = $('.js-userform-submit-fail');

            board.controls.$user_modal.addClass('m-show');
            board.positionModal( board.controls.$user_modal );

            $phone_input.focus(  watchPhone );
            $name_input.focus(   watchName  );
            $email_input.focus(  watchEmail );
            $rules_input.change( watchRules );
            $rules_link.click( board.showRules );

            Inputmask.extendAliases({
              'userPhone': {
                  mask: '+7 (999) 999-99-99',
                  placeholder: '_',
                  showMaskOnHover: false
              }
            });

            $phone_input.inputmask('userPhone');

            function errorCheck() {

                var flag = true;

                for (var key in input_states) {
                    if (input_states[key] == false) {
                        flag = false;
                    }
                }

                if (flag) {
                   $userform_submit.prop('disabled', false);
                } else {
                   $userform_submit.prop('disabled', true);
                }
            }

            function watchName() {

                $name_input.blur(function(){
                    var name = $name_input.val();

                    if ( name.length < 2 || nameCheck(name) !== -1) {
                        $name_input.addClass('m-error');
                        input_states.name = false;
                    } else {
                        input_states.name = true;
                    }

                    $name_input.keyup(function(){
                        var name = $name_input.val();

                        if ( name.length < 2 || nameCheck(name) !== -1 ) {
                            $name_input.addClass('m-error');
                            input_states.name = false;
                        } else {
                            $name_input.removeClass('m-error');
                            input_states.name = true;
                        }
                        errorCheck();
                    });
                    errorCheck();
                });


                function nameCheck(value) {
                    var reg = /([^а-яА-ЯёЁa-zA-Z-\s]+)/;
                    return value.search( reg );
                }
            };

            function watchEmail() {

                $email_input.blur(function(){
                    var email = $email_input.val();

                    if ( emailCheck(email) == -1 ) {
                        $email_error.html('Некорректный e-mail. Пример: example@example.ru');
                        $email_input.addClass('m-error');
                        input_states.email = false;
                    } else {
                        $email_input.removeClass('m-error');
                        input_states.email = true;
                    }

                    $email_input.keyup(function(){
                        var email = $email_input.val();

                        if ( emailCheck(email) == -1 ) {
                            $email_error.html('Некорректный e-mail. Пример: example@example.ru');
                            $email_input.addClass('m-error');
                            input_states.email = false;
                        } else {
                            $email_input.removeClass('m-error');
                            input_states.email = true;
                        }
                        errorCheck();
                    });
                    errorCheck();
                });

                function emailCheck(value) {

                    /** Выполняет поиск сопоставления между регулярным выражением и
                      * переданной строкой. В качестве маски используется
                      * General Email Regex (RFC 5322 Official Standard).
                      *
                      * @param {str} email address
                      * @return {int} индекс при совпадении
                      * @return {int} -1 если строка не попадает под regexp
                      */

                    var reg = /(^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$)/;
                    return value.search(reg);
                }
            }

            function watchPhone() {

                $phone_input.blur(function(evt){

                    function clearValue() {
                        var input = $phone_input.val(),
                            input = input.replace(/(\+[7])/,''),
                            input = input.replace(/\D/g,''),
                            input = input.substring(0,11);
                        return input
                    }

                    var input = clearValue();


                    if (input.length == 0 || input.length == 10) {
                      $phone_input.removeClass('m-error');
                      input_states.phone = true;
                    } else {
                      $phone_input.addClass('m-error');
                      input_states.phone = false;
                    }

                    $phone_input.keyup(function(){

                      var input = clearValue();

                      if (input.length == 0 || input.length == 10) {
                        $phone_input.removeClass('m-error');
                        input_states.phone = true;
                      } else {
                        $phone_input.addClass('m-error');
                        input_states.phone = false;
                      }

                      errorCheck();
                    });

                    errorCheck();
                });
            };

            function watchRules(evt) {
                if ( $(evt.currentTarget).is( ":checked" ) ){
                    input_states.rules = true;
                } else {
                    input_states.rules = false;
                }
                errorCheck();
            };

            function emailExists() {
                $email_error.html('Введенный <br>e-mail уже <br>участвовал в акции.');
                $email_input.addClass('m-error');
                input_states.email = false;
            }


            var formData = '';


            $userform_submit.click(function(evt){
                board.dataLayerEvent('Interactions', 'send', 'successesBoard', board.vars.context.name, board.vars.content, null);
                sendForm(evt);
            });

            $userform_pseudo.click(function(evt){
                board.dataLayerEvent('Interactions', 'send', 'failBoard', board.vars.context.name, board.vars.content, null);
            });


            function sendForm() {
                evt.preventDefault();

                $userform_submit.off();
                $userform_submit.prop('disabled', true);

                $userform_input.each(function(index, input){

                    var name  = $(input).attr('name'),
                        value = $(input).val();

                    formData += name + '=' + value;

                    if (index < $userform_input.length - 1) {
                        formData += '&'
                    }
                });

                if ( source == 'notest' ) {
                    formData += '&sku=';
                    formData += board.vars.context.sku;
                }

                $.ajax({
                    url: '/ajax/lb/user',
                    data: formData,
                    contentType: 'application/x-www-form-urlencoded',
                    processData: false,
                    method: 'POST'
                }).done(
                    function(data) {
                        board.controls.$user_modal.removeClass('m-show');
                        board.controls.$success_modal.addClass('m-show');
                        board.positionModal(board.controls.$success_modal);
                        board.dataLayerEvent('Non-Interactions', 'show', 'LetterSent', board.vars.context.name, board.vars.content, null);

                        var $success_close = $('.js-success-close');

                        $success_close.click( function(){
                            board.controls.$success_modal.removeClass('m-show');
                            $success_close.off();
                        });


                }).fail(
                    function(jqXHR){
                        if (jqXHR.status == '418') {
                            $userform_submit.click(sendForm);
                            emailExists();
                        } else {
                            console.log( 'Request failed with status ' + jqXHR.status + ': ' + jqXHR.statusText );
                            board.controls.$user_modal.removeClass('m-show');
                            board.controls.$error_modal.addClass('m-show');
                            board.positionModal(board.controls.$error_modal);
                            board.dataLayerEvent('Non-Interactions', 'show', 'LetterFails', board.vars.context.name, board.vars.content, null);
                        }
                });
            }
        },

        positionModal: function(element) {
            $(element).css('opacity', 1);
        },

        showPreloader: function() {
            board.controls.$load_modal.addClass('m-show');
            board.positionModal(board.controls.$load_modal);
        },

        showRules: function(evt) {
            evt.preventDefault();

            var $rules_close = $('.js-rules-close');

            var height = window.innerHeight - 200;

            board.controls.$rules_modal
                .addClass('m-show')
                .css('height', height )
                .css('opacity', 1)

            $rules_close.click(function(){
                evt.preventDefault();
                board.controls.$rules_modal.removeClass('m-show');
                $rules_close.off();
            });
        },

        dataLayerEvent: function(evtCategory, evtAction, evtLabel, evtContext, evtContent, evtPosition = null) {
            dataLayer.push({
                'event': 'OWOX',
                'eventCategory':      evtCategory,
                'eventAction':        evtAction,
                'eventLabel':         evtLabel,
                'eventContext':       evtContext,
                'eventContent':       evtContent,
                'eventPosition':      evtPosition,
                'eventLocation':      'lp_cutting_board',
                'productsQuantityTotal':  null,
                'eventPortal':        null,
                'eventCategoryName':  null,
                'eventCategoryId':    null,
                'eventProductName':   null,
                'eventProductId':     null,
                'eventProductPrice':  null,
                'ecommerce':          null
            })
        }
    }

    
    board.init();

});
