/**
 * Скрипт для новогодней промо-страницы с товарами
 */

$(document).ready(function() {
    'use strict';

    setTimer();
    magicBall();


    var activeBlock = null;
    var activePlus = null;
    var basketGlobal = [];

    getFixedCard();
    getPlus('tree-11');
    getPlus('tree-2');


    function getFixedCard() {

        var url = '/fixed-cart/content';
        $.ajax({
            url: url,
            type: "GET",
            dataType: 'json'
        })
        
        .done(function (result) {
            if (result.total_count > 0) {
                var price_rub = result.total_price.toString().split('.')[0];
                var price_kopek = result.total_price.toString().split('.')[1];

                if (!price_kopek) {
                    price_kopek = '00';
                } else if (price_kopek.toString().length === 1) {
                    price_kopek = price_kopek + '0';
                }

                $('.ny2018-main-head__cart-count').html(result.total_count);
                $('.ny2018-main-head__cart-value').html(price_rub + '<sup>' + price_kopek +'</sup>');
                $('.ny2018-main-head__cart').addClass('m-show');
            }
        })
        .fail(function () {

        });
    }

    function getPlus(idTree) {



        var plusTree = $('#' + idTree).find('.ny-tree-plus__item');
        var treePlusProduct = {};

        $.each(plusTree, function(i) {

            var plusID = $( plusTree[i] ).attr( 'id' );
            var plusProductsList = $( plusTree[i] ).find( '.ny-tree-plus__button' ).data('product');
            var plusProducts;

            if (typeof plusProductsList  === 'number' ) {
              plusProductsList = plusProductsList.toString();
            }

            if ( typeof plusProductsList  !== 'undefined' ) {
              plusProducts = plusProductsList.split( ',' );
              treePlusProduct[ plusID ] = plusProducts;
            }

        });

        getProductForPlus( treePlusProduct );
    }


    function getProductForPlus(object, productBlock = 'undefined') {

        var query = JSON.stringify( object );

        var url = '/js-api/ny2018/check_available_items?items=' + query;

        $.ajax({
            url: url,
            type: "GET",
            dataType: 'json'
        })
        .done(function (result) {

            for (var key in result) {
                var productBlock = $( '#' + key ).find( '.ny-tree-plus__product' );
                var plusProduct = result[ key ];

                renderProduct(plusProduct, productBlock);
                productEvent(productBlock);
            }

            $.each(basketGlobal, function(i) {
                updateData(basketGlobal[i], $('[data-id="' + basketGlobal[i].code + '"]'));
            });
        })
        .fail(function () {

        });
    }

    $('.ny-tree-plus__button').on('click', function(evt) {
        var listPlus = $(evt.currentTarget).parents('.ny-tree-plus');
        if ($(listPlus).find('.ny-tree-plus__item.m-pulse')) {
            $(listPlus).find('.ny-tree-plus__item.m-pulse').removeClass('m-pulse');
        }
    	var buttonPlus = evt.currentTarget;
    	var buttonWrapper = $(buttonPlus).parent();
    	var productArray = $(buttonPlus).data('product');
    	var productBlock = $(buttonWrapper).find('.ny-tree-plus__product');

    	if (activePlus === buttonPlus) {
    		closePlus();
    	} else {
    		if (activeBlock !== null) {
	    		closePlus();
	    	}
    		openPlus(buttonPlus, buttonWrapper)
    	}
    });

    $('.ny-tree-plus').on('click', function(evt) {
        if ($(evt.target).hasClass('ny-tree-plus')) {
            closePlus();
        }
    });

    function closePlus() {
        $(activeBlock).removeClass('m-open');
        activeBlock = null;
        activePlus = null;
    }

    function openPlus(button, block) {
        $(block).addClass('m-open');
        activeBlock = block;
        activePlus = button;
    }

    function parseResult(data) {
		$.each(data, function(i) {
			data[i].price_format = transformPrice(data[i].price_format);
		});

		return data;
	}

    function transformPrice(value) {
        var pattern = new RegExp('(\\d)(?=(\\d{' + 3 + '})+(?!\\d))', 'g');
        value = Math.floor(value).toString().replace(pattern, '$1' + (' ' || ' '));
        return value;
    }

    function updateData(data, productBlock) {
        $('[data-id="' + data.code + '"]').data('value', data.cartQuantity);
        var htmlQuantity = $('[data-id="' + data.code + '"]').find('.html-quantity');
        var htmlPrice = $('[data-id="' + data.code + '"]').find('.html-price');
        var htmlPriceCent = $('[data-id="' + data.code + '"]').find('.html-price-cent');
        if (data.weighted) {
            var htmlKg = $('[data-id="' + data.code + '"]').find('.html-kg');
            var htmlG = $('[data-id="' + data.code + '"]').find('.html-g');

            $.each(htmlKg, function(i) {
                $(htmlKg[i]).val(data.cart_kg);
                $(htmlG[i]).val(data.cart_g);
            });
        } else {
            var htmlCount = $('[data-id="' + data.code + '"]').find('.html-count');

            $.each(htmlCount, function(i) {
                $(htmlCount[i]).val(data.cart_count);
            });
        }

        $.each(htmlQuantity, function(i) {
            $(htmlQuantity[i]).html(data.quantity);
            $(htmlPrice[i]).html(data.price_format);
            $(htmlPriceCent[i]).html(data.price_format_cent);
        });

        if (data.cartQuantity > 0) {
            $('[data-id="' + data.code + '"]').addClass('m-cart');
        } else {
            $('[data-id="' + data.code + '"]').removeClass('m-cart');
            $('[data-id="' + data.code + '"]').addClass('m-remove');

            setTimeout(function() {
                $('[data-id="' + data.code + '"]').removeClass('m-remove');
            }, 2000);
        }
    }

    function productEvent(block) {
        $(block).on('click', '.ny-product__button', function(evt) {
            var $item = $(evt.currentTarget).parents('.ny-product');
            var data = {};

            data.productCodePost = $item.data('id');
            if ($item.data('type') === 'weighted') {
                data.qty = 500;
            } else {
                data.qty = 1;
            }

            addBasket(data);
        });

        $(block).on('click', '.ny-product-cart__button--plus', function(evt) {
            var $item = $(evt.currentTarget).parents('.ny-product');
            var data = {};
            var value = parseInt($item.data('value'));

            data.productCodePost = $item.data('id');
            if ($item.data('type') === 'weighted') {
                value += 100;

                if (value > 99999) {
                    value = 99999;
                }

                var htmlKg = $('[data-id="' + data.productCodePost + '"]').find('.html-kg');
                var htmlG = $('[data-id="' + data.productCodePost + '"]').find('.html-g');

                $.each(htmlKg, function(i) {
                    $(htmlKg[i]).val(parseInt(value/1000));
                    $(htmlG[i]).val(value%1000);
                });

            } else {
                value += 1;

                if (value > 99) {
                    value = 99;
                }

                var htmlCount = $('[data-id="' + data.productCodePost + '"]').find('.html-count');

                $.each(htmlCount, function(i) {
                    $(htmlCount[i]).val(value);
                });
            }

            $('[data-id="' + data.productCodePost + '"]').data('value', value);

            data.qty = value;

            addBasket(data);
        });

        $(block).on('click', '.ny-product-cart__button--minus', function(evt) {
            var $item = $(evt.currentTarget).parents('.ny-product');
            var data = {};
            var value = parseInt($item.data('value'));

            data.productCodePost = $item.data('id');

            if ($item.data('type') === 'weighted') {
                value -= 100;

                if (value < 100) {
                    value = 100;
                }

                var htmlKg = $('[data-id="' + data.productCodePost + '"]').find('.html-kg');
                var htmlG = $('[data-id="' + data.productCodePost + '"]').find('.html-g');

                $.each(htmlKg, function(i) {
                    $(htmlKg[i]).val(parseInt(value/1000));
                    $(htmlG[i]).val(value%1000);
                });

            } else {
                value -= 1;

                if (value < 1) {
                    value = 1;
                }

                var htmlCount = $('[data-id="' + data.productCodePost + '"]').find('.html-count');

                $.each(htmlCount, function(i) {
                    $(htmlCount[i]).val(value);
                });
            }

            $('[data-id="' + data.productCodePost + '"]').data('value', value);

            data.qty = value;

            addBasket(data);
        });

        $(block).on('click', '.ny-product-cart__remove', function(evt) {
            var $item = $(evt.currentTarget).parents('.ny-product');
            var data = {};

            data.productCodePost = $item.data('id');

            data.qty = 0;

            addBasket(data);
        });

        $(block).on('change', '.html-count', function(evt) {
            var $item = $(evt.currentTarget).parents('.ny-product');
            var data = {};

            data.productCodePost = $item.data('id');

            var value = $(evt.currentTarget).val();

            if (value < 1 || value === '') {
                value = 1;
            } else if (value > 99) {
                value = 99;
            }

            var htmlCount = $('[data-id="' + data.productCodePost + '"]').find('.html-count');

            $.each(htmlCount, function(i) {
                $(htmlCount[i]).val(value);
            });

            data.qty = parseInt(value);

            $('[data-id="' + data.productCodePost + '"]').data('value', data.qty);

            addBasket(data);
        });

        $(block).on('change', '.html-kg', function(evt) {
            var $item = $(evt.currentTarget).parents('.ny-product');
            var data = {};

            data.productCodePost = $item.data('id');

            var valueKg = $(evt.currentTarget).val();
            var valueG = $item.find('.html-g').val();

            if (valueKg < 1 || valueKg === '') {
                valueKg = 0;
                if (valueG < 100) {
                    valueG = 100;
                }
            } else if (valueKg > 99) {
                valueKg = 99;
            }

            var htmlKg = $('[data-id="' + data.productCodePost + '"]').find('.html-kg');
            var htmlG = $('[data-id="' + data.productCodePost + '"]').find('.html-g');

            $.each(htmlKg, function(i) {
                $(htmlKg[i]).val(valueKg);
                $(htmlG[i]).val(valueG);
            });

            data.qty = parseInt(valueKg*1000) + parseInt(valueG);

            $('[data-id="' + data.productCodePost + '"]').data('value', data.qty);

            addBasket(data);
        });

        $(block).on('change', '.html-g', function(evt) {
            var $item = $(evt.currentTarget).parents('.ny-product');
            var data = {};

            data.productCodePost = $item.data('id');

            var valueG = $(evt.currentTarget).val();
            var valueKg = $item.find('.html-kg').val();

            if (valueG < 1 || valueG === '') {
                if (valueKg < 1) {
                    valueG = 100;
                } else {
                    valueG = 0;
                }
            } else if (valueG > 0 && valueG < 999) {
                if (valueKg < 1 && valueG < 100) {
                    valueG = 100;
                }
            } else if (valueG > 999) {
                valueG = 999;
            }

            var htmlKg = $('[data-id="' + data.productCodePost + '"]').find('.html-kg');
            var htmlG = $('[data-id="' + data.productCodePost + '"]').find('.html-g');

            $.each(htmlKg, function(i) {
                $(htmlKg[i]).val(valueKg);
                $(htmlG[i]).val(valueG);
            });

            data.qty = parseInt(valueKg*1000) + parseInt(valueG);

            $('[data-id="' + data.productCodePost + '"]').data('value', data.qty);

            addBasket(data);
        });
    }

    function addBasket(data) {
        data = data || {};

        $('[data-id="' + data.productCodePost + '"]').addClass('m-load');

        $.post('/cart/updateQuantity', data).done(function (result) {
            console.log(result);
            result = parseData(result.product);
            updateData(result, $('[data-id="' + data.productCodePost + '"]'));
            $('[data-id="' + data.productCodePost + '"]').removeClass('m-load');
            window.updateHybrisBasket();
        });

        getFixedCard();
    }

    function parseData(data) {
        data.price_format = data.totalPrice.toString().split('.')[0];
        data.price_format = transformPrice(data.price_format);
        data.price_format_cent = data.totalPrice.toString().split('.')[1];

        if (!data.price_format_cent) {
            data.price_format_cent = '00';
        } else if (data.price_format_cent.length === 1) {
            data.price_format_cent = '0' + data.price_format_cent;
        }

        if (!data.weighted) {
            data.cart_count = data.cartQuantity;
            data.quantity = data.cart_count + ' шт. ';
        } else {
            if (data.cartQuantity > 999) {
                data.cart_kg = parseInt(data.cartQuantity/1000);
                data.quantity = data.cart_kg + ' кг. ';
                if (data.cartQuantity%1000 > 0) {
                    data.cart_g = data.cartQuantity % 1000;
                    data.quantity += data.cart_g % 1000 + ' г. '
                } else {
                    data.cart_g = 0;
                }
            } else {
                data.cart_kg = 0;
                data.cart_g = data.cartQuantity;
                data.quantity = data.cartQuantity + ' г. ';
            }
        }

        return data;
    }

    function getBasketHybris() {
        $.get('/cart/json-entries').then(function (result) {
            $.each(result, function(i) {
                result[i] = parseData(result[i]);
                updateData(result[i], $('[data-id="' + result[i].code + '"]'));
            });
        });
    }

    function renderProduct(data, productBlock) {
    	$(productBlock).empty();
    	$.tmpl('productTpl', data).appendTo($(productBlock));
    }

    $.template('productTpl',
    	'<article class="ny-product" data-id="${id}" data-type="${item_type}">' +
			'<a class="ny-product__image-link" href="${link}" title="${name}">' +
				'<img src="${photo_card}" alt="${name}" width="189" height="189"/>' +
			'</a>' +
			'<div class="ny-product__content">' +
				'<h2 class="ny-product__title">' +
					'<a class="ny-product__link" href="${link}" title="${name}">${name}</a>' +
				'</h2>' +
				'<div class="ny-product__price-wrapper">' +
					'<div class="ny-product__price"><b>${price_format}</b><sup>${price_format_cent}</sup></div>' +
					'<div class="ny-product__measure">за ${price_format_style}</div>' +
				'</div>' +

                '{{if is_alcohol === 0 && available_to_order === 1 }}'+
                    '<button class="ny-product__button"><span></span></button>'+
                '{{/if}}'+

			'</div>' +
            '<div class="ny-product__cart ny-product-cart">' +
                '<button class="ny-product-cart__remove" type="button">' +
                    '<span class="ny-product-cart__remove-tip">Удалить из корзины</span>' +
                '</button>' +
                '<h3 class="ny-product-cart__title">Отличный выбор!</h3>' +
                '<a class="ny-product-cart__link" href="${link}" title="${name}">${name}</a>' +
                '<div class="ny-product-cart__info">' +
                    '<div class="ny-product-cart__text">Добавили <b class="html-quantity"></b> в корзину ' +
                        '<div class="ny-product-cart__price">на <b class="html-price"></b><sup class="html-price-cent"></sup> руб.,</div> ' +
                    'изменить количество?</div>' +
                    '<div class="ny-product-cart__count">' +
                        '<button class="ny-product-cart__button ny-product-cart__button--minus" type="button"></button>' +
                        '{{if item_type === "weighted"}}' +
                            '<div class="ny-product-cart__weight">' +
                                '<input class="html-kg" type="number">' +
                                '<span>кг.</span>' +
                            '</div>' +
                            '<div class="ny-product-cart__weight">' +
                                '<input class="html-g" type="number">' +
                                '<span>г.</span>' +
                            '</div>' +
                        '{{else}}' +
                            '<div class="ny-product-cart__weight">' +
                                '<input class="html-count" type="number">' +
                                '<span>шт.</span>' +
                            '</div>' +
                        '{{/if}}' +
                        '<button class="ny-product-cart__button ny-product-cart__button--plus" type="button"></button>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="ny-product__remove ny-product-cart__remove-msg">Товар удален из корзины</div>' +
		'</article>');


    // Оживление теста

    var testData = {
       1:{
          'question':'Начнем с главного: <br /> вы уже чувствуете аромат <br /> мандаринов в воздухе?',
          'options':[
             'Конечно!',
             'Нет тут никаких мандаринов :('
          ],
          'reply':[
             '<span></span> Добавьте к мандариновому настроению еще и ананасовое!<br /> В Китае верят, что ананас  приносит успех и процветание :)',
             '<span></span> У нас нет волшебной палочки, но есть волшебная зеленая кнопка с корзинкой ;) <br /> Одно нажатие, и мандариновое настроение приедет к вам куда угодно!'
          ],
          'products':[
             [ '093540', '250956', '247175' ],
             [ '280138', '212672', '283493', '352169', '377155' ]
          ]
       },
       2:{
          'question':'Новогодняя елка уже ждет своего <br/> звездного часа у вас дома?',
          'options':[
             'Да',
             'Нет у меня елки'
          ],
          'reply':[
             '<span></span> Она не должна ждать в одиночестве! Подарите ей верного друга – символ наступающего года :)',
             '<span></span> Самое время ее выбрать!<br/> Даже ходить никуда не нужно, все здесь же, в «Азбуке Вкуса»:'
          ],
          'products':[
             [ '368962', '369168', '364586', '369171' ],
             [ '378048', '378050', '378052', '249979' ]
          ]
       },
       3:{
          'question':'Как насчет игрушек? <br /> Уже обновляли коллекцию в этом году?',
          'options':[
             'Конечно',
             'Еще нет'
          ],
          'reply':[
             '<span></span> Вашим новогодним настроением уже можно заряжать окружающих! Осталось только  добавить ему огоньков:',
             '<span></span> А приходите к нам сегодня! <br /> Или прямо сейчас нажимайте ' +
             '<a href="https://av.ru/new_year2018/sparkle/" target="_blank" class="ny2018-quest__answer-link">сюда</a> ' +
             ' и выбирайте самые красивые игрушки :)'
          ],
          'products':[
             [ '229587', '376993', '276769', '197865' ],
             [ ]
          ]
       },
       4:{
          'question':'Переходим к самому вкусному! <br /> Вы уже знаете, что будете готовить?',
          'options':[
             'Да, у меня все спланировано!',
             'Пока не до этого'
          ],
          'reply':[
             '<span></span> Вот бы к вам в гости попасть! <br /> Для неожиданных гостей, кстати, <br /> всегда можно заказать утку с яблоками.  Она фантастически вкусная, а как выглядит… ',
             '<span></span> Мы вам поможем. ' +
             '<a href="https://av.ru/new_year2018/dishes/simple/" target="_blank" class="ny2018-quest__answer-link">Тут</a> '+
             ' готовое банкетное меню на 6 или 10 человек – быстро и ни о чем  думать не надо!'
          ],
          'products':[
             [ '148543' ],
             [ ]
          ]
       },
       5:{
          'question':'Торт или десерт – вот в чем вопрос!',
          'options':[
             'Конечно, торт',
             'Десерт! Люблю новое и оригинальное'
          ],
          'reply':[
             '<span></span> Согласны! Мы тоже не представляем, как можно отказаться от торта с очаровательным Дедом Морозом!',
             '<span></span> У нас точно есть то, что вам нужно! Бисквитное полено с кремом из маскарпоне и мармеладный Дед Мороз!'
          ],
          'products':[
             [ '374015' ],
             [ '336777' ]
          ]
       },
       6:{
          'question':'Наконец-то дошли до подарков! <br /> Вы уже придумали, что дарить? ',
          'options':[
             'Давно!',
             'До сих пор ломаю голову'
          ],
          'reply':[
             '<span></span> А про коллег не забыли?  А про учителей и консьержа? :)<br /> Если что, '+
             '<a href="https://av.ru/new_year2018/podarki-new/" target="_blank" class="ny2018-quest__answer-link">тут живут</a> '+
             ' идеи подарков!',
             '<span></span> Посвятите время себе, а за подарками отправьте нас! Мы уже все подготовили и упаковали, осталось только ' +
             '<a href="https://av.ru/new_year2018/podarki-new/" target="_blank" class="ny2018-quest__answer-link">выбрать</a>'
          ],
          'products':[

          ]
       },
       7:{
          'question':'Что выберете для завершающего <br /> штриха в интерьере?',
          'options':[
             'Снежный шар',
             'Новогоднюю свечу'
          ],
          'reply':[
             '<span></span> Как в детстве! <br /> Нам тоже очень они нравятся! ',
             '<span></span> В душе вы романтик! <br /> Пожалуй, вам стоит запастись красивыми  свечами еще и на Рождество!'
          ],
          'products':[
             [ '368999', '369004', '369012' ],
             [ '369046', '369047', '230697', '230698', '379272', '369027' ]
          ]
       }
    };



    moronTest();


    function moronTest() {

      var testResult = JSON.parse( $( '.js-test-result' ).text() );
      // var testData = JSON.parse( $( '.js-test-data' ).text() );

      // GLOBALS

      var counter = 1;
      var score = 0;
      var moronScore;

      var activePanel;
      var nextPanel;
      var userAnswer;

      var productList = [];

      var shareRedirectUrl;

      // Получить список товаров
      getProducts();

      // Инициализировать тест
      renderPanels();


      function getProducts() {

          // Определяем скрытый родительский контейнер
          var testProductContainer = $( '.ny2018-test__hidden-goods' );

          $( testProductContainer ).empty();

          var productList = {};

          // Получаем список всех необходимых товаров и рендерим их
          for (var key in testData) {

            $.each( testData[key][ 'products' ], function( index, array ) {

              if ( array.length !== 0 ) {

                  // Определяем общий ID
                  var productId = 'question' + '-' + key + '-' + index;

                  // Создаем разметку для рендера товаров
                  $( testProductContainer ).append( $('<div>', { id: productId }) );
                  var container = $( '#' + productId );
                  $( container).append( $( '<div>', { class: 'ny-tree-plus__product' } ) );

                  // Генерируем список товаров для запроса

                  productList[ productId ] = array;
                }
            });
          }

          getProductForPlus( productList );
      }


      function renderPanels() {

          // VARIABLES:

          // Блоки теста
          activePanel = setActivePanel( counter );
          nextPanel = setActivePanel( counter + 1 );

          var answerSubmitBtn = $( activePanel ).find( '.js-submit-answer' )[0];

          // Лейблы с вариантами ответа
          var labels = $.makeArray( $(activePanel).find( '.ny2018-quest__label' ) );

          // RENDER:
          // Отобразить тексты вопросов на панелях
          $( activePanel ).find( '.ny2018-test__question' ).html( testData[ counter ][ 'question' ] );

          if (counter + 1 < 8) {
            $( nextPanel ).find( '.ny2018-test__question' ).html( testData[ counter + 1 ][ 'question' ] );
          } else {
            $( nextPanel ).find( '.ny2018-test__question' ).html( '' );
          }


          // Отобразить варианты ответа на активной панели
          $.each( labels, function( index, item ) {
              $( item ).html( testData[ counter ][ 'options' ][ index ] );
          });

          $( activePanel ).addClass( 'm-test');

          $( answerSubmitBtn ).on( 'click', submitAnswer );

      }


      function submitAnswer( event ) {

        // Удалить обработчик с кнопки
        $(event.target).off( 'click', submitAnswer );

        // Посчитать балл за ответ
        userAnswer = $( activePanel ).find( '.ny2018-quest__input:checked' ).val();

        if ( userAnswer == 0 ) {
          score += 1;
        };

        // Вернуть радиоинпут на место (если необходимо)
        var defaultInput = $( activePanel ).find( '.ny2018-quest__input' )[0];

        if ( $( defaultInput ).is(':checked') === false ) {
            $( defaultInput ).prop('checked', true);
        }

        // Найти кнопку "Следующий вопрос"
        var nextQuestionBtn = $( activePanel ).find( '.js-next-question' )[0];

        // Подготовить заготовочку ответа
        $( activePanel ).find( '.ny2018-quest__answer' ).html( testData[ counter ][ 'reply' ][ userAnswer ] );


        // Удалить товары из предыдущего ответа
        var productWrapper = $( activePanel ).find( '.ny2018-quest__product-wrapper' );
        $( productWrapper ).empty();

        // Если для этого ответа есть товар
        var hiddenProduct = $( '#question' + '-' + counter + '-' + userAnswer ).find( 'article' );

        if ( hiddenProduct !== 'undefined' ) {

            productEvent( hiddenProduct );
            $( hiddenProduct ).detach().appendTo( productWrapper );
        }


        if ( counter == 7 ) {

          // Если последний вопрос - переопределить кнопку
          $( nextQuestionBtn ).html( 'Узнать результат' );
          $( activePanel ).removeClass( 'm-test');
          $( activePanel ).addClass( 'm-answer');
          $( nextQuestionBtn ).on( 'click', showResult );

        } else {

          // Если нет - показать следующий вопрос
          $( activePanel ).removeClass( 'm-test');
          $( activePanel ).addClass( 'm-answer');
          $( nextQuestionBtn ).on( 'click', nextQuestion );
        }
      }


      function nextQuestion( event ) {

          $( event.target ).off( 'click', nextQuestion );

          $( activePanel ).removeClass( 'm-answer');

          counter += 1;

          renderPanels();
      }


      function showResult( event ) {

        // Удалить обработчик и вернуть исходный текст в кнопку
        $( event.target ).html( 'Cледующий вопрос' );
        $( event.target ).off( 'click', showResult );

        var resultMsg;
        var resultImg;


        // Считаем результат
        if (score == 0) {
          moronScore = 0;
          shareRedirectUrl = 'pitahaya';

        } else if (score <= 2) {
          moronScore = 30;
          shareRedirectUrl = 'iris';

        } else if ( score <= 4 ) {
          moronScore = 70;
          shareRedirectUrl = 'absinth';

        } else {
          moronScore = 100;
          shareRedirectUrl = 'mandarine';
        }

        // Подставить текст ответа
        $( '.ny2018-test__result' ).find( '.ny2018-test__result-heading' ).html( testResult[ moronScore ][ 'result-heading' ] );
        $( '.ny2018-test__result' ).find( '.ny2018-test__result-text' ).html( testResult[ moronScore ][ 'result-text' ] );

        // Показать картинку
        $( '.ny2018-test__result-img' + testResult[ moronScore ][ 'image' ] ).addClass( 'm-show' );

        // Показать проценты
        $( '.ny2018-test__result-progress-text' ).html( moronScore + '%' );

        if ( moronScore <= 40 ) {
          $( '.ny2018-test__result-progress-text' ).addClass( 'm-blue' );
        } else {
          $( '.ny2018-test__result-progress-text' ).removeClass( 'm-blue' );
        }

        // и прогресс-бар

        // 1. Считаем длину
        var progBarWidth = function() {
            // maxWidth = 570px
            var width = Math.round( 570 / 100 * moronScore );
            return width.toString() + 'px';
        }

        // 2. Скругляем уголки при необходимости
        if ( moronScore == 100 ) {
          $( '.ny2018-test__result-progress-bar' ).addClass( 'm-full-round');
        }
        // 3. Прописываем длину
        $( '.ny2018-test__result-progress-bar' ).css( 'width', progBarWidth );



        // Спрятать активную панель
        $( activePanel ).removeClass( 'm-answer');

        // Спрятать блок с вопросами
        $( '.ny2018-test__questions-wrapper' ).addClass( 'm-hide');

        // Показать финальный результат
        $( '.ny2018-test__result' ).addClass( 'm-show');

        // Кнопки
        var repeatTestBtn = $('.ny2018-test__result-repeat');
        var supplyTestBtn = $('.ny2018-test__result-supply');

        $( repeatTestBtn ).on( 'click', repeatTest );

        $( supplyTestBtn ).on( 'click', function() {
          window.location.href = "https://av.ru/collections/tovary-dlya-ng";
        });

        // Шеринг
        $('.js-test-share-fb').on( 'click', shareCallback );
        $('.js-test-share-vk').on( 'click', shareCallback );
        $('.js-test-share-tw').on( 'click', shareCallback );
      }


      function shareCallback( evt ) {

        var source = evt.currentTarget;
        var service;

        if ( $( source ).hasClass( 'js-test-share-vk' ) ) {
          service = 'vk';
        } else if ( $( source ).hasClass( 'js-test-share-fb' ) ) {
          service = 'facebook';
        } else if ( $( source ).hasClass( 'js-test-share-tw' ) ) {
          service = 'twitter';
        }

        testShare( service, shareRedirectUrl );
      }


      function testShare( service, pathname ) {

        var title = 'Тест «Насколько вы готовы к Новому году?»';
        var url = 'https://av.ru/new_year2018/' + pathname + '/';
        var share_url;


        switch( service ) {
            case 'facebook':
                share_url = 'https://www.facebook.com/dialog/feed?app_id=145634995501895&display=popup';
                share_url += '&link=' + encodeURIComponent(url);
                break;

            case 'twitter':
                share_url = 'https://twitter.com/intent/tweet?';
                // share_url += 'text='+ encodeURIComponent(title);
                share_url += '&url='+ encodeURIComponent(url);
                break;

            case 'vk':
                share_url = 'https://vk.com/share.php?';
                share_url += '&url='          + encodeURIComponent(url);
                share_url += '&noparse=false'
                break;
            }

        sharePopup( share_url )

        function sharePopup( url ) {
          window.open(url,'','toolbar=0,status=0,width=626,height=436');
        }
    }


    function repeatTest ( event ) {

        // Удалить обработчик
        $( event.target ).off( 'click', repeatTest );

        $('.js-test-share-fb').off( 'click', shareCallback );
        $('.js-test-share-vk').off( 'click', shareCallback );
        $('.js-test-share-tw').off( 'click', shareCallback );

        // Спрятать блок c результатом и удалить модификаторы
        $( '.ny2018-test__result' ).removeClass( 'm-show');
        $( '.ny2018-test__result-img').removeClass( 'm-show' );
        $( '.ny2018-test__result-progress-bar' ).removeClass( 'm-full-round');

        // Показать блок с вопросами
        $( '.ny2018-test__questions-wrapper' ).removeClass( 'm-hide');

        // Сбросить переменные
        counter = 1;
        score = 0;
        moronScore = 0;

        // Получить список товаров
        getProducts();

        renderPanels();
    }


    function setActivePanel( counter ) {

      var panel;

      switch( counter ) {
          case 1:
          case 3:
          case 5:
          case 7:
              panel = $( '.ny2018-test__quest--left' );
              break

          case 2:
          case 4:
          case 6:
          case 8:
              panel = $( '.ny2018-test__quest--right' );
              break

          default:
              panel = $( '.ny2018-test__quest--left' );
              break
      }
        return panel[0];
    }
}


    // Установка таймера страницы
    function setTimer() {

        var placeholder = $('.ny2018-oldstyle__header-title');
        var lead = $('.js-page-lead');

        $( placeholder ).html( '' );

        var postNYText =     'Со старым новым годом!';

        var leadTextBefore = 'Новый год встретили, а на очереди старый Новый год! ' +
                             'Значит, продолжаем ходить в гости, <br />' +
                             'любоваться елкой и делать друг другу сюрпризы. ' +
                             'Нажмите на волшебный шар ниже и узнайте, <br />' +
                             'что мы приготовили для вас!';

        var leadTextAfter =  'Пусть в вашем сердце всегда живет любовь: к людям, ' +
                             'к миру, маленьким сладостям и сочным фруктам! А мы ' +
                             'сделаем все, чтобы наша любовь приносила вам радость ' +
                             'каждый день. Нажмите на волшебный шар ниже и узнайте, ' +
                             'что мы приготовили для вас!';


        var timeLeft = getDifference();

        var substLeft;
        var substCount;
        var substUnit;

        if ( timeLeft === undefined ) {
            $( placeholder ).html( postNYText );
            $( lead ).html( leadTextAfter );
        } else if ( timeLeft <= 24 ) {
            setHours( timeLeft );
            $( lead ).html( leadTextBefore );
        } else {
            setDays( timeLeft );
            $( lead ).html( leadTextBefore );
        }


        function setDays( hrs ) {

            var template =
                  'До старого нового года ' +
                  '<span class="js-days-left">осталось</span> ' +
                  '<span class="ny2018-main-head__title-text_number js-days-count">13</span> ' +
                  '<span class="js-days-inc">дней</span>!'

            $( placeholder ).html( template );

            var daysInit = 14;
            var daysLeft = Math.ceil( hrs / 24 ) - 1;
            var duration = 1800; //milliseconds
            var frame = Math.round( duration / (daysInit - daysLeft) );
            var units = [ 'день', 'дня', 'дней' ];

            substLeft = inclineDaysLeft( daysLeft );
            substCount = daysLeft;
            substUnit = inclineUnits( daysLeft, units );

            var countPlaceholder = $( '.js-days-count' );

            var daysTimer = setInterval(function() {
                if ( daysInit > daysLeft ) {
                    -- daysInit;
                    $(countPlaceholder).html( daysInit );
                } else {
                    clearInterval(daysTimer);
                    renderText();
                }
            }, frame )

        }


        function setHours( hrs ){

          var template =
                'До старого нового года ' +
                '<span class="js-days-left">осталось</span> ' +
                '<span class="ny2018-main-head__title-text_number js-days-count">24</span> ' +
                '<span class="js-days-inc">часа</span>!'

          $( placeholder ).html( template );

          var hrsInit = 24;
          var hrsLeft = hrs;
          var duration;

          if ( hrs <= 12 ) {
              duration = 1800;
          } else {
              duration = 1000;
          }

          var duration = 1200; //milliseconds
          var frame = Math.round( duration / (hrsInit - hrsLeft) );
          var units = [ 'час', 'часа', 'часов' ];

          substLeft = inclineDaysLeft( hrsLeft );
          substCount = hrsLeft;
          substUnit = inclineUnits( hrsLeft, units );

          var countPlaceholder = $( '.js-days-count' );

          var daysTimer = setInterval(function() {
              if ( hrsInit > hrsLeft ) {
                  -- hrsInit;
                  $(countPlaceholder).html( hrsInit );
              } else {
                  clearInterval(daysTimer);
                  renderText();
              }
          }, frame )
        }


        function getDifference() {

            // returns time to NY in hrs based on client time
            // or nothing

            var NY = new Date('2018', '00', '14', '00', '00');
            var today = new Date();
            var diff = NY.getTime() - today.getTime();

            if (diff > 0) {
                return Math.ceil( diff / (1000 * 60 * 60));
            }
        }


        function renderText() {

            $( '.js-days-count').html( substCount );
            $( '.js-days-left' ).html( substLeft );
            $( '.js-days-inc' ).html( substUnit );
        }


        function inclineDaysLeft( number ) {
            var incline = ['остался', 'осталось']

            if (number == 1 || number == 21 || number == 31 ) {
              return incline[0];
            } else {
              return incline[1];
            }
        }


        function inclineUnits( number, words ) {
             var cases = [2, 0, 1, 1, 1, 2];
             return words[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
        }
    }


    // Промокоды

    function magicBall() {

        var predictions = {};
        var magicNBall = $('.ny2018-main-yourself__snow');
        var repeatBtn = $( '.js-predict-repeat' );
        var addPromocodeBtn = $('.js-predict-cart');


        if ( $( '.ny2018-promo__item' ).length !== 0 ) {

          getPredictions();
          console.log( 'Promocodes succesfully loaded' );

          $( magicNBall ).on('click', makePredict);

        } else {
          console.log( 'No promocodes found' );
        }


        function getPredictions() {

            // Подгружаем промокоды и заголовки к ним из админки
            // и удаляем из разметки

            var promoSource = $( '#promocodes');

              $.each( $( promoSource ).find( '.ny2018-promo__item' ), function( index, item ) {

                  var prophecy = {};

                  prophecy.code = $( item ).children( '.js-promo-code' ).text();
                  prophecy.text = $( item ).children( '.js-promo-text' ).text();
                  prophecy.image = $( item ).children( '.js-promo-image-url' ).text();

                  predictions[index] = prophecy;
              });

              $( promoSource ).empty();

        } //getPredictions


        function getRandomIndex( obj ) {

            // Выбираем рандомный индекс из списка предсказаний
            return Math.floor( Math.random() * Object.keys( obj ).length );

        }  // getRandomIndex

        function makePredict( event ) {

          // disable clicks
          $( magicNBall ).off('click', makePredict);
          $( repeatBtn ).off('click', makePredict);
          $( addPromocodeBtn ).unbind('click');

          var idx = getRandomIndex( predictions );
          var url = 'https://av.ru/cart/?promo=' + predictions[idx]['code'];



          if ( $(event.target).attr('id') === 'repeat__link') {
              // Попробовать ещё раз
              $( '.ny2018-magicball__snow_fast').addClass( 'm-show' );
              $( '.ny2018-main-yourself__product').addClass( 'm-fade' );
              $( '.ny2018-main-yourself__product').removeClass( 'm-show' );
              $( '.ny2018-main-yourself__getgift').removeClass( 'm-show' );

              var waitASec = setTimeout(function(){

                  $( '.js-predict-text' ).html( predictions[idx]['text'] );
                  $( '.js-product-image' ).attr( 'src', '');
                  $( '.ny2018-main-yourself__product').removeClass( 'm-show m-fade' );
                  $( '.js-product-image' ).attr( 'src', predictions[idx]['image'] );
                  $( '.ny2018-magicball__snow_fast').removeClass( 'm-show' );
                  $( '.ny2018-main-yourself__getgift').addClass( 'm-show' );
                  $( '.ny2018-main-yourself__product').addClass( 'm-show' );
                  $( addPromocodeBtn ).on('click', sendPromocode);
                  $( repeatBtn ).on('click', makePredict);
              }, 2000)


          } else {
              // Первый  клик
              $( '.js-predict-text' ).html( predictions[idx]['text'] );
              $( '.js-product-image' ).attr( 'src', predictions[idx]['image'] );
              $( '.ny2018-magicball__snow_fast').addClass( 'm-show' );
              $( '.ny2018-magicball__magic-tree').addClass( 'm-hide' );

              var waitASec = setTimeout(function(){
                  $( '.ny2018-magicball__snow_fast').removeClass( 'm-show' );
                  $( '.ny2018-main-yourself__getgift').addClass( 'm-show' );
                  $( '.ny2018-main-yourself__product').addClass( 'm-show' );
                  $( addPromocodeBtn ).on('click', sendPromocode);
                  $( repeatBtn ).on('click', makePredict);
              }, 2000)
          }

          function sendPromocode(evt) {

              var xhr = new XMLHttpRequest();
              var msg = 'Ура, товар добавлен в вашу корзину! ' +
                        'Вы можете перейти по этой ' +
                        '<a href="https://av.ru/cart/" title="Перейти в корзину">ссылке</a>, ' +
                        'чтобы увидеть его, или продолжить покупки в интернет-магазине ' +
                        '<a href="https://av.ru" title="Вернуться на главную">av.ru</a>.';




              xhr.open('GET', url, true);
              xhr.send();

              if (xhr.status != 200) {
                // обработать ошибку
                console.log( xhr.status + ': ' + xhr.statusText );
                $( addPromocodeBtn ).addClass('m-disabled');
                $( '.js-predict-repeat' ).addClass('m-hide');
                $( '.js-predict-text' ).html( msg );

              } else {
                // вывести результат
                $( addPromocodeBtn ).addClass('m-disabled');
                $( '.js-predict-repeat' ).addClass('m-hide');
                $( '.js-predict-text' ).html( msg );
              }


          }

        } // makePredict
    } // magicBall

});
