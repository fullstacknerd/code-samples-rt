import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FeedService } from './../../feed/feed.service';
import { AppGlobal } from './../../../service/global';
import { Storage } from './../../../service/storage';
import { GoogleAnalitics } from './../../../service/ga';
import { AppComponent } from './../../../app-component';

import * as _ from 'lodash';

@Component({
    selector: 'main.app-content',
    templateUrl: './campaign-select-full.html'
})

export class CampaignSelectFull {
    mainFeed: any;
    errorFeed: boolean;
    id: string;
    item: any;
    load: boolean;
    campaign_list: Array<string> = [];
    select_button_text: string = 'Выберите предложения';
    message_text: string = '';
    showModal: boolean;
    loadBuy: boolean;
    feedCurrentPage: number;
    feedTotalPages: number;

    constructor(
        private appComponent: AppComponent,
        private appGlobal: AppGlobal,
        private googleAnalitics: GoogleAnalitics,
        private storage: Storage,
        private feedService: FeedService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.appGlobal.scrollToTop();
        this.mainFeed = this.storage.getData('main-feed', '') || '';
        this.route.params.subscribe(params => this.id = params.id);

        if (!this.mainFeed) {
            this.getMainFeed();
        } else {
            this.load = true;
            this.item = _.find(this.mainFeed, {id: this.id }) || null;
            this.googleAnalitics.viewFeedItem(this.item.skin);
            this.load = false;
        }
    }

    getMainFeed(): any {
        let sessionId = this.storage.getData('session-id', '');
        let skinList = this.storage.getData('skin-list', '') || '';
        this.errorFeed = false;
        this.load = true;

        this.feedService.mainScreen(sessionId, skinList)
            .then(response => {
                this.mainFeed = response.data.feed_unit_list;
                this.item = _.find(this.mainFeed, {id: this.id }) || null;
                this.googleAnalitics.viewFeedItem(this.item.skin);
                this.load = false;
                this.storage.setDataForTime('main-feed', this.mainFeed, '', 600000);
            })
            .catch(error => {
                this.errorFeed = true;
                this.load = false;
            });
    }

    goBack() {
        this.appGlobal.goBack();
    }

    selectCampaign(id: string) {
        let campaign = document.getElementById(id) as HTMLElement;
        let campaign_button = campaign.querySelector('.js-campaign-button') as HTMLElement;
        let index = this.campaign_list.indexOf(id);

        if (campaign.classList.contains('m-select')) {
            if (index !== -1) {
                campaign.classList.remove('m-select');
                campaign_button.innerHTML = 'Выбрать предложение';
                this.campaign_list.splice(index, 1);
            }
        } else {
            if (index === -1) {
                if (this.campaign_list.length < this.item.select_max) {
                    campaign.classList.add('m-select');
                    campaign_button.innerHTML = 'Выбрано';
                    this.campaign_list.push(id);
                }
            }
        }

        if (this.campaign_list.length > 0) {
            this.select_button_text = 'Выбрано ' + this.campaign_list.length + ' ' + this.appGlobal.getWordByNumber(this.campaign_list.length, ['предложение', 'предложения', 'предложений']);
            this.message_text = this.appGlobal.getWordByNumber(this.campaign_list.length, ['это', 'эти', 'этих']) + ' ' + this.campaign_list.length + ' ' + this.appGlobal.getWordByNumber(this.campaign_list.length, ['предложение', 'предложения', 'предложений']);
        } else {
            this.select_button_text = 'Выберите предложения';
            this.message_text = '';
        }
    }
    
    slideItems(direction) {
        let scrollList = document.querySelector('.js-select-list') as HTMLElement;
        let scrollItems = scrollList.querySelectorAll('li');
        let itemsTotal = scrollItems.length;
        
        let step = 4; // Шаг в 4 карточки
        let width = 235; // Ширина карточки 220px + margin 15px
        let offset;
        
        // Присваиваем значения переменным класса 
        // при инициализации страницы
        if (!this.feedTotalPages) {
            this.feedTotalPages = Math.ceil(itemsTotal / step);  // Всего страниц в слайдере
            this.feedCurrentPage = 1;   // Текущая страница слайдера
        }
        
        if (direction === 'left') {
          
            // скроллим назад
            if (this.feedCurrentPage > 1) {
                this.feedCurrentPage--; 
            }
            
        } else {
          
            // скроллим вперед          
            if (this.feedCurrentPage < this.feedTotalPages) {
                this.feedCurrentPage++;
            }  
        }
        
        if (this.feedCurrentPage === 1) {
            
            // если первая страница - сдвиг 0
            offset = 0;
        } else if (this.feedCurrentPage === this.feedTotalPages) {
            
            // если страница последняя - 
            // показываем последние 4 карточки
            offset = ((itemsTotal - step) * width) * -1;
        } else {
            
            // если страница не первая и не последняя,
            // рассчитываем отступ от номера страницы
            offset = this.feedCurrentPage * (step * width) * -1;
        }
        
        scrollList.style.transform = 'translateX(' + offset + 'px)';
    }

    openModal() {
        this.showModal = true;
    }

    closeModal() {
        this.showModal = false;
    }

    buyFeed(feedId: string) {
        let token = this.storage.getData('token', '');
        this.loadBuy = true;

        this.feedService.campaignSelect(token, feedId, this.campaign_list)
            .then(response => {
                this.loadBuy = false;
                this.buySuccess();
            })
            .catch(error => {
                this.loadBuy = false;
                this.appComponent.showAlert = true;
                this.appComponent.alert = {
                    title: 'Ошибка',
                    text: error.error.error_message
                };
            });
    }

    buySuccess() {
        this.storage.removeData('main-feed', '');
        this.goBack();
    }
}
