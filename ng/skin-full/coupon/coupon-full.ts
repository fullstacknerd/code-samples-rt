import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FeedService } from './../../feed/feed.service';
import { AppGlobal } from './../../../service/global';
import { Storage } from './../../../service/storage';
import { GoogleAnalitics } from './../../../service/ga';
import { AppComponent } from './../../../app-component';

import * as _ from 'lodash';
import * as JsBarcode from 'jsbarcode';

@Component({
    selector: 'main.app-content',
    templateUrl: './coupon-full.html'
})

export class CouponFull {
    mainFeed: any;
    id: string;
    item: any;
    loadItem: boolean;
    couponData: any = {
        info: false,
        message: false
    };
    couponInvalid: boolean = true;
    settingButton: any = {
        title: 'Перейти к карте магазинов',
        online: false
    };
    loadCoupon: boolean = false;

    constructor(
        private appGlobal: AppGlobal,
        private googleAnalitics: GoogleAnalitics,
        private storage: Storage,
        private feedService: FeedService,
        private router: Router,
        private route: ActivatedRoute,
        private appComponent: AppComponent) { }

    ngOnInit(): void {
        this.appGlobal.scrollToTop();
        this.mainFeed = this.storage.getData('main-feed', '') || '';
        this.route.params.subscribe(params => this.id = params.id);

        if (!this.mainFeed) {
            this.getMainFeed();
        } else {
            this.loadItem = true;
            this.item = _.find(this.mainFeed, {id: this.id }) || null;
            if (this.item.store_filter) {
                if (this.item.store_filter.type === 'store_type') {
                    if (this.item.store_filter.store_type.length === 1 && this.item.store_filter.store_type[0] === 'online') {
                        this.settingButton = {
                            title: 'Перейти на av.ru',
                            online: true
                        }
                    }
                }
            }
            this.googleAnalitics.viewFeedItem(this.item.skin);
            this.loadItem = false;
            this.renderCode();
        }
    }

    getMainFeed(): any {
        let token = this.storage.getData('token', '');
        let skinList = this.storage.getData('skin-list', '') || '';
        this.loadItem = true;

        this.feedService.mainScreen(token, skinList)
            .then(response => {
                this.mainFeed = response.data.feed_unit_list;
                this.item = _.find(this.mainFeed, {id: this.id }) || null;
                if (this.item.store_filter) {
                    if (this.item.store_filter.type === 'store_type') {
                        if (this.item.store_filter.store_type.length === 1 && this.item.store_filter.store_type[0] === 'online') {
                            this.settingButton = {
                                title: 'Перейти на av.ru',
                                online: true
                            }
                        }
                    }
                }
                this.googleAnalitics.viewFeedItem(this.item.skin);
                this.loadItem = false;
                this.renderCode();
                this.storage.setDataForTime('main-feed', this.mainFeed, '', 600000);
            })
            .catch(error => {
                this.googleAnalitics.event('Ошибка при запросе ленты');
                this.loadItem = false;
                this.appComponent.showAlert = true;
                this.appComponent.alert = {
                    title: 'Ошибка',
                    text: error.error.error_message
                };
            });
    }

    changeCheckbox(event: any, type: string) {
        this.couponData[type] = event.target.checked;

        if (this.couponData.info === true && this.couponData.message === true) {
            this.couponInvalid = false;
        } else {
            this.couponInvalid = true;
        }
    }

    getCoupon(id: string) {
        let token = this.storage.getData('token', '');
        this.loadCoupon = true;

        this.feedService.getCoupon(token, id)
            .then(response => {
                this.loadCoupon = false;
                this.item = response.data;
                this.renderCode();
                this.appComponent.showResultMessage('Вы получили купон на скидку!');
            })
            .catch(error => {
                this.googleAnalitics.event('Ошибка при получении купона');
                this.loadCoupon = false;
                this.appComponent.showAlert = true;
                this.appComponent.alert = {
                    title: 'Ошибка',
                    text: error.error.error_message
                };
            });
    }

    renderCode() {
        if (this.item.barcode) {
            setTimeout(() => {
                JsBarcode('#code-' + this.item.barcode, this.item.barcode, {
                    format: 'code128c',
                    displayValue: false
                });
            }, 0);
        }
    }

    goStore() {
        if (this.settingButton.online) {
            this.appGlobal.goLink('https://av.ru/');
        } else {
            this.item.store_filter.data = {
                type: 'купон на скидку',
                title: this.item.title,
                url: '/coupon/' + this.item.id
            }
            this.storage.setData('store-filter-feed', this.item.store_filter, '');
            this.appGlobal.goUrl('/stores');
        }
    }

    goBack() {
        this.appGlobal.goBack();
    }
}
