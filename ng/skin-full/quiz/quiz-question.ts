import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FeedService } from './../../feed/feed.service';
import { AppGlobal } from './../../../service/global';
import { Storage } from './../../../service/storage';
import { AppComponent } from './../../../app-component';
import { GoogleAnalitics } from './../../../service/ga';

import * as _ from 'lodash';

import * as moment from 'moment';

@Component({
    selector: 'main.app-content',
    templateUrl: './quiz-question.html'
})

export class QuizQuestion {
    mainFeed: any;
    errorFeed: boolean;
    id: string;
    number: string;
    item: any;
    load: boolean;
    loadQuiz: boolean;
    questionList: any;
    quizIcon: any;
    question: any;
    buttonNext: boolean;
    userAnswerId: string;
    questionId: string;
    play_timer: any;
    timer_progress: string;
    time: any;
    timeout: any;
    timeoutValue: any;
    minute: any;
    second: any;
    loadFinish: boolean;
    nextDisabled: boolean;

    constructor(
        private appComponent: AppComponent,
        private appGlobal: AppGlobal,
        private googleAnalitics: GoogleAnalitics,
        private storage: Storage,
        private feedService: FeedService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.route.params.subscribe(params => this.id = params.id);
        this.quizIcon = this.storage.getData('quiz-icon_' + this.id,  '');
        this.questionList = this.storage.getData('quiz-question_' + this.id, '');
        this.timeout = this.storage.getData('quiz-timeout_' + this.id, '');
        this.time = this.timeout;
        this.route.params.subscribe(params => this.number = params.number);
        this.question = this.questionList[parseFloat(this.number) - 1];
        this.loadSaveAnswer(this.number);
        this.parseTime();

        this.buttonNext = true;
        
        // Введен атрибут блокировки кнопки для того, чтобы предотвратить
        // множественные запросы (VK-1463)
        this.nextDisabled = false;
        //
    }

    goNext() {
        let number = parseFloat(this.number);

        if (number < this.questionList.length) {
            number++;
            this.question = this.questionList[number - 1];
            this.number = number.toString();
            this.loadSaveAnswer(this.number);
            this.appGlobal.goUrl('quiz/' + this.id + '/' + number);
        } else {
            this.nextDisabled = true;
            this.finishQuiz();
        }
    }

    goBack() {
        let number = parseFloat(this.number);

        if (number > 1) {
            number--;
            this.question = this.questionList[number - 1];
            this.number = number.toString();
            this.loadSaveAnswer(this.number);
            this.appGlobal.goUrl('quiz/' + this.id + '/' + number);
        } else {
            clearTimeout(this.play_timer);
            this.appGlobal.goBack();
        }
    }

    changeInput(event: any) {
        let input = event.target;
        let type = input.dataset.type;
        this.userAnswerId = input.dataset.value;
        this.questionId = input.name;

        if (type === 'radio') {
            let inputList = Array.prototype.slice.call(document.querySelectorAll('.js-input'), 0);
            inputList.forEach(function (inputItem: any) {
                if (inputItem !== input) {
                    inputItem.checked = false;
                }
            });
        }

        if (input.checked) {
            this.buttonNext = false;
        } else {
            this.buttonNext = true;
        }
    }

    disabledAnswer() {
        let inputList = Array.prototype.slice.call(document.querySelectorAll('.js-input'), 0);
        inputList.forEach(function (inputItem: any) {
            inputItem.disabled = true;
        });
    }

    checkAnswer() {
        this.googleAnalitics.quiz('Проверка ответа');
        this.disabledAnswer();

        let token = this.storage.getData('token', '');

        this.feedService.checkAnswer(token, this.id, this.questionId, this.userAnswerId)
            .then(response => {
                this.googleAnalitics.quiz('Успешная проверка ответа');
                this.markAnswer(response);
            })
            .catch(error => {
                this.googleAnalitics.quiz('Ошибка при проверке ответа');
                this.appComponent.showAlert = true;
                this.appComponent.alert = {
                    title: 'Ошибка',
                    text: error.error_message
                };
            });
    }

    markAnswer(data: any) {
        let input_true;
        let input_false;

        input_true = document.getElementById('input-' + data.true_answer) as HTMLInputElement;
        input_true.classList.add('m-true');

        if (data.true_answer !== data.user_answer) {
            input_false = document.getElementById('input-' + data.user_answer) as HTMLInputElement;
            input_false.classList.add('m-false');
            this.googleAnalitics.quiz('Неверный ответ');
        } else {
            this.googleAnalitics.quiz('Верный ответ');
        }

        this.saveAnswers(data);

        this.buttonNext = true;
    }

    saveAnswers(data: any) {
        let quiz_save = this.storage.getData('quiz_' + this.id, '') || [];
        let number = parseFloat(this.number)

        quiz_save[number - 1] = data;

        this.storage.setData('quiz_' + this.id, quiz_save, '');
    }

    loadSaveAnswer(number: string) {
        let quiz_save = this.storage.getData('quiz_' + this.id, '') || [];
        let data = quiz_save[parseFloat(number) - 1];
        if (data) {
            setTimeout(() => {
                this.disabledAnswer();
                this.markAnswer(data);
            }, 0);
        }
    }

    finishQuiz() {
        this.googleAnalitics.quiz('Завершение викторины');
        clearTimeout(this.play_timer);
        let token = this.storage.getData('token', '');
        this.loadFinish = true;

        this.feedService.finishQuiz(token)
            .then(response => {
                this.googleAnalitics.quiz('Успешное завершение викторины');
                this.loadFinish = false;
                this.storage.setData('quiz-result_' + this.id, response, '');
                this.appGlobal.goUrl('quiz-result/' + this.id);
            })
            .catch(error => {
                this.googleAnalitics.quiz('Ошибка при завершении викторины');
                this.loadFinish = false;
                this.appComponent.showAlert = true;
                this.appComponent.alert = {
                    title: 'Ошибка',
                    text: error.error_message
                };
            });
    }

    endTime() {
        this.googleAnalitics.quiz('Завершение викторины по окончанию времени');
        let token = this.storage.getData('token', '');
        this.loadFinish = true;

        this.feedService.endTimeQuiz(token)
            .then(response => {
                this.googleAnalitics.quiz('Успешное завершение викторины по окончанию времени');
                this.loadFinish = false;
                this.storage.setData('quiz-result_' + this.id, response, '');
                this.appGlobal.goUrl('quiz-result/' + this.id);
            })
            .catch(error => {
                this.googleAnalitics.quiz('Ошибка при завершении викторины по окончанию времени');
                this.loadFinish = false;
                this.appComponent.showAlert = true;
                this.appComponent.alert = {
                    title: 'Ошибка',
                    text: error.error_message
                };
            });
    }

    getProgressBar() {
      this.minute = Math.floor(this.time / 60);
      this.second  = this.time % 60;

      if (this.minute < 1) {
        this.minute = 0;
      }

      if (this.minute < 10) {
        this.minute = '0' + this.minute;
      }

      if (this.second < 10) {
        this.second = '0' + this.second;
      }
    }

    parseTime() {
        this.timer_progress = (this.time / this.timeout * 100).toFixed(2);

        if (this.time > 0) {
          this.play_timer = setTimeout(() => {
            this.time = --this.time;
            this.parseTime();
            this.getProgressBar();
          }, 1000);
        } else {
           clearTimeout(this.play_timer);
           this.endTime();
        }
    }
}
