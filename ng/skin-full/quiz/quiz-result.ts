import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FeedService } from './../../feed/feed.service';
import { AppGlobal } from './../../../service/global';
import { Storage } from './../../../service/storage';
import { GoogleAnalitics } from './../../../service/ga';

import * as _ from 'lodash';

@Component({
    selector: 'main.app-content',
    templateUrl: './quiz-result.html'
})

export class QuizResult {
    id: string;
    result: any;

    constructor(
        private appGlobal: AppGlobal,
        private googleAnalitics: GoogleAnalitics,
        private storage: Storage,
        private feedService: FeedService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.googleAnalitics.viewPage('Результат викторины');
        this.route.params.subscribe(params => this.id = params.id);
        this.result = this.storage.getData('quiz-result_' + this.id, '') || '';
        if (this.result.bonus_amount) {
            this.result.more_text = this.appGlobal.getWordByNumber(this.result.bonus_amount, ['бонус получен', 'бонуса получено', 'бонусов получено']);
        } else if (this.result.marks_amount) {
            this.result.more_text = this.appGlobal.getWordByNumber(this.result.marks_amount, ['марка получена', 'марки получено', 'марок получено']);
        }
    }

    goBack() {
        this.storage.removeData('quiz_' + this.id,  '');
        this.storage.removeData('quiz-icon_' + this.id,  '');
        this.storage.removeData('quiz-question_' + this.id, '');
        this.storage.removeData('quiz-timeout_' + this.id, '');
        this.storage.removeData('quiz-result_' + this.id, '');
        this.storage.removeData('main-feed', '');

        this.appGlobal.goUrl('/main');
    }
}
