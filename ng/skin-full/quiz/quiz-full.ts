import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AppGlobal } from './../../../service/global';
import { FeedService } from './../../feed/feed.service';
import { Storage } from './../../../service/storage';
import { AppComponent } from './../../../app-component';
import { GoogleAnalitics } from './../../../service/ga';

import * as _ from 'lodash';

@Component({
    selector: 'main.app-content',
    templateUrl: './quiz-full.html'
})

export class QuizFull {
    mainFeed: any;
    errorFeed: boolean;
    id: string;
    item: any;
    load: boolean;
    error: boolean;
    loadQuiz: boolean;

    constructor(
        private appComponent: AppComponent,
        private appGlobal: AppGlobal,
        private googleAnalitics: GoogleAnalitics,
        private storage: Storage,
        private feedService: FeedService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.appGlobal.scrollToTop();
        this.mainFeed = this.storage.getData('main-feed', '') || '';
        this.route.params.subscribe(params => this.id = params.id);

        this.getInfo(this.id);
    }

    goBack() {
        this.appGlobal.goBack();
    }

    getInfo(id: string) {
        this.googleAnalitics.quiz('Загрузка викторины');
        let token = this.storage.getData('token', '');
        this.load = true;
        this.error = false;

        this.feedService.getQuizInfo(token, id)
            .then(response => {
                this.item = response;
                this.googleAnalitics.viewFeedItem(this.item.skin);
                this.googleAnalitics.quiz('Успешная загрузка викторины');
                this.load = false;
            })
            .catch(error => {
                this.googleAnalitics.quiz('Ошибка при загрузке викторины');
                this.load = false;
                this.error = true;
                this.appComponent.showAlert = true;
                this.appComponent.alert = {
                    title: 'Ошибка',
                    text: error.error.error_message
                };
            });
    }

    startQuiz(id: string) {
        this.googleAnalitics.quiz('Старт викторины');
        let token = this.storage.getData('token', '');
        this.load = true;

        this.feedService.startQuiz(token, id)
            .then(response => {
                this.googleAnalitics.quiz('Успешный старт викторины');
                this.storage.setData('quiz-icon_' + this.id, this.item.icon_url,  '');
                this.storage.setData('quiz-question_' + this.id, response.question_list,  '');
                this.storage.setData('quiz-timeout_' + this.id, response.timeout,  '');
                this.appGlobal.goUrl('/quiz/' + this.id + '/1');
                this.load = false;
            })
            .catch(error => {
                this.googleAnalitics.quiz('Ошибка при старте викторины');
                this.load = false;
                this.appComponent.showAlert = true;
                this.appComponent.alert = {
                    title: 'Ошибка',
                    text: error.error.error_message
                };
            });
    }
}
