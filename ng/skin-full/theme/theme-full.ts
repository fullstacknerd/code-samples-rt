import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FeedService } from './../../feed/feed.service';
import { AppGlobal } from './../../../service/global';
import { Storage } from './../../../service/storage';
import { GoogleAnalitics } from './../../../service/ga';
import { AppComponent } from './../../../app-component';

import * as _ from 'lodash';

@Component({
    selector: 'main.app-content',
    templateUrl: './theme-full.html'
})

export class ThemeFull {
    mainFeed: any;
    errorFeed: boolean;
    id: string;
    item: any;
    load: boolean;
    loadBuy: boolean;
    showModal: boolean;
    showAlert: boolean;
    alert: any;

    constructor(
        private appComponent: AppComponent,
        private appGlobal: AppGlobal,
        private googleAnalitics: GoogleAnalitics,
        private storage: Storage,
        private feedService: FeedService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.appGlobal.scrollToTop();
        this.mainFeed = this.storage.getData('main-feed', '') || '';
        this.route.params.subscribe(params => this.id = params.id);

        if (!this.mainFeed) {
            this.getMainFeed();
        } else {
            this.load = true;
            this.item = _.find(this.mainFeed, {id: this.id }) || null;
            this.googleAnalitics.viewFeedItem(this.item.skin);
            this.load = false;
        }
    }

    getMainFeed(): any {
        let token = this.storage.getData('token', '');
        let skinList = this.storage.getData('skin-list', '') || '';
        this.errorFeed = false;
        this.load = true;

        this.feedService.mainScreen(token, skinList)
            .then(response => {
                this.mainFeed = response.data.feed_unit_list;
                this.item = _.find(this.mainFeed, {id: this.id }) || null;
                this.googleAnalitics.viewFeedItem(this.item.skin);
                this.storage.setDataForTime('main-feed', this.mainFeed, '', 600000);
                this.load = false;
            })
            .catch(error => {
                this.errorFeed = true;
                this.load = false;
            });
    }

    goBack() {
        this.appGlobal.goBack();
    }

    buyFeed(feedId: string) {
        this.googleAnalitics.theme('Подписка');
        let token = this.storage.getData('token', '');
        this.loadBuy = true;

        this.feedService.subscribe(token, feedId)
            .then(response => {
                this.loadBuy = false;
                this.buySuccess();
            })
            .catch(error => {
                this.googleAnalitics.theme('Ошибка при подписке');
                this.loadBuy = false;
                this.appComponent.showAlert = true;
                this.appComponent.alert = {
                    title: 'Ошибка',
                    text: error.error.error_message
                };
            });
    }

    buySuccess() {
        this.googleAnalitics.theme('Успешная подписка');
        this.storage.removeData('main-feed', '');
        this.appGlobal.goUrl('/main');
    }

    deepLink(link_unit: any) {
        if (link_unit) {
            this.appGlobal.deepLink(link_unit.link_code, link_unit.link_unit);
        }
    }

    goMission(i: number, status: string) {
      console.log(status);
      if (status !== 'unavailable') {
        this.appGlobal.goUrl('/theme/' + this.item.id + '/' + i);
      }
    }
}
