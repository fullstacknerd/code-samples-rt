import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FeedService } from './../../feed/feed.service';
import { AppGlobal } from './../../../service/global';
import { Storage } from './../../../service/storage';
import { GoogleAnalitics } from './../../../service/ga';

import * as _ from 'lodash';

@Component({
    selector: 'main.app-content',
    templateUrl: './theme-mission-full.html'
})

export class ThemeMissionFull {
    mainFeed: any;
    errorFeed: boolean;
    id: string;
    task: number;
    item: any;
    mission: any;
    load: boolean;

    constructor(
        private appGlobal: AppGlobal,
        private googleAnalitics: GoogleAnalitics,
        private storage: Storage,
        private feedService: FeedService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.appGlobal.scrollToTop();
        this.mainFeed = this.storage.getData('main-feed', '') || '';
        this.route.params.subscribe(params => this.id = params.id);
        this.route.params.subscribe(params => this.task = params.task);

        if (!this.mainFeed) {
            this.getMainFeed();
        } else {
            this.load = true;
            this.item = _.find(this.mainFeed, {id: this.id }) || null;
            this.mission = this.item.tasks[this.task];
            // this.googleAnalitics.viewFeedItem(this.item.skin);
            this.load = false;
        }
    }

    getMainFeed(): any {
        let token = this.storage.getData('token', '');
        let skinList = this.storage.getData('skin-list', '') || '';
        this.errorFeed = false;
        this.load = true;

        this.feedService.mainScreen(token, skinList)
            .then(response => {
                this.mainFeed = response.data.feed_unit_list;
                this.item = _.find(this.mainFeed, {id: this.id }) || null;
                this.mission = this.item.tasks[this.task];
                // this.googleAnalitics.viewFeedItem(this.item.skin);
                this.storage.setDataForTime('main-feed', this.mainFeed, '', 600000);
                this.load = false;
            })
            .catch(error => {
                this.errorFeed = true;
                this.load = false;
            });
    }

    goBack() {
        this.appGlobal.goBack();
    }

    deepLink(link_unit: any) {
        this.appGlobal.deepLink(link_unit.link_code, link_unit.link_unit);
    }
}
