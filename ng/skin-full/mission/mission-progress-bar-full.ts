import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FeedService } from './../../feed/feed.service';
import { AppGlobal } from './../../../service/global';
import { Storage } from './../../../service/storage';

import * as _ from 'lodash';

@Component({
    selector: 'main.app-content',
    templateUrl: './mission-progress-bar-full.html'
})

export class MissionProgressBarFull {
    mainFeed: any;
    errorFeed: boolean;
    id: string;
    item: any;
    load: boolean;

    constructor(
        private appGlobal: AppGlobal,
        private storage: Storage,
        private feedService: FeedService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.appGlobal.scrollToTop();
        this.mainFeed = this.storage.getData('main-feed', '') || '';
        this.route.params.subscribe(params => this.id = params.id);

        if (!this.mainFeed) {
            this.getMainFeed();
        } else {
            this.load = true;
            this.item = _.find(this.mainFeed, {id: this.id }) || null;
            this.item.percent = (this.item.progress / this.item.progress_max * 100).toFixed();
            this.item.help_text = 'Осталось накопить ' + this.appGlobal.transformBalance(this.item.progress_max - this.item.progress) + ' из ' + this.appGlobal.transformBalance(this.item.progress_max) + ' ' + this.appGlobal.getWordByNumber(this.item.progress_max, ['бонус', 'бонуса', 'бонусов']);
            this.load = false;
        }
    }

    getMainFeed(): any {
        let token = this.storage.getData('token', '');
        let skinList = this.storage.getData('skin-list', '') || '';
        this.errorFeed = false;
        this.load = true;

        this.feedService.mainScreen(token, skinList)
            .then(response => {
                this.mainFeed = response.data.feed_unit_list;
                this.item = _.find(this.mainFeed, {id: this.id }) || null;
                this.item.percent = (this.item.progress / this.item.progress_max * 100).toFixed();
                this.item.help_text = 'Осталось накопить ' + this.appGlobal.transformBalance(this.item.progress_max - this.item.progress) + ' из ' + this.appGlobal.transformBalance(this.item.progress_max) + ' ' + this.appGlobal.getWordByNumber(this.item.progress_max, ['бонус', 'бонуса', 'бонусов']);
                this.load = false;
                this.storage.setDataForTime('main-feed', this.mainFeed, '', 600000);
            })
            .catch(error => {
                this.errorFeed = true;
                this.load = false;
            });
    }

    goBack() {
        this.appGlobal.goBack();
    }
}