import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FeedService } from './../../feed/feed.service';
import { AccountService } from './../../account/account.service';
import { AppGlobal } from './../../../service/global';
import { Storage } from './../../../service/storage';
import { AppComponent } from './../../../app-component';

import * as _ from 'lodash';

@Component({
    selector: 'main.app-content',
    templateUrl: './recipe-step.html'
})

export class RecipeStep {
    mainFeed: any;
    errorFeed: boolean;
    id: string;
    item: any;
    load: boolean;
    step: string;
    recipeItem: any;
    recipeList: any;
    showEmailModal: boolean;
    emailInvalid: boolean;
    loadEmail: boolean;

    constructor(
        private appComponent: AppComponent,
        private appGlobal: AppGlobal,
        private storage: Storage,
        private feedService: FeedService,
        private router: Router,
        private route: ActivatedRoute,
        private accountService: AccountService) { }

    ngOnInit(): void {
        this.appGlobal.scrollToTop();
        this.load = true;
        this.route.params.subscribe(params => this.id = params.id);
        this.route.params.subscribe(params => this.step = params.step);

        this.mainFeed = this.storage.getData('main-feed', '');
        this.recipeItem = _.find(this.mainFeed, {id: this.id }) || null;
        this.recipeList = this.recipeItem.steps;

        this.item = this.recipeList[parseFloat(this.step) - 1];
        this.item.percent = ((parseFloat(this.step) / this.recipeList.length) * 100).toFixed(2);

        this.load = false;
    }

    goBack() {
        if (parseFloat(this.step) !== 1) {
            let number = parseFloat(this.step) - 1;
            this.item = this.recipeList[number - 1];
            this.item.percent = (number / this.recipeList.length * 100).toFixed(2);
            this.appGlobal.goUrl('/recipe/' + this.id + '/' + number);
        } else {
            this.appGlobal.goUrl('/recipe/' + this.id);
        }
    }

    goNext() {
        let number = parseFloat(this.step) + 1;
        this.item = this.recipeList[number - 1];
        this.item.percent = (number / this.recipeList.length * 100).toFixed(2);
        this.appGlobal.goUrl('/recipe/' + this.id + '/' + number);
    }

    checkEmail() {
      let email = this.storage.getData('account', '').email || '';

      if (!email) {
          this.showEmailModal = true;
          document.getElementById('input-email').focus();
      } else {
        this.emailRecipe();
      }
    }

    saveEmail() {
        this.showEmailModal = false;

        let inputEmail = document.getElementById('input-email') as HTMLFormElement;
        let account = this.storage.getData('account', '') || '';
        let formData: any = {
            first_name: account.first_name || '',
            last_name: account.last_name || '',
            email: inputEmail.value
        };
        let token = this.storage.getData('token', '');
        this.loadEmail = true;

        this.accountService.editProfile(token, formData)
          .then(response => {
              this.loadEmail = false;
              this.storage.setData('account', response.data.account, '');
              this.appComponent.account = response.data.account;
              this.emailRecipe();
          })
          .catch(error => {
              this.loadEmail = false;
              this.appComponent.showAlert = true;
              this.appComponent.alert = {
                  title: 'Ошибка',
                  text: error.error.error_message
              };
          });
    }

    inputEmail(event: any) {
        let regex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

        if (regex.test(event.target.value.trim())) {
            this.emailInvalid = false;
        } else {
            this.emailInvalid = true;
        }
    }

    emailRecipe() {
        let token = this.storage.getData('token', '');
        let account = this.storage.getData('account', '');
        this.loadEmail = true;

        this.feedService.emailRecipe(token, this.id, account.email.email)
            .then(response => {
                this.loadEmail = false;
                this.appComponent.showResultMessage('Рецепт отправлен на почту');
            })
            .catch(error => {
                this.loadEmail = false;
                this.appComponent.showAlert = true;
                this.appComponent.alert = {
                    title: 'Ошибка',
                    text: error.error.error_message
                };
            });
    }

    closeEmailModal() {
      this.showEmailModal = false;
    }
}
