import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FeedService } from './../../feed/feed.service';
import { AppGlobal } from './../../../service/global';
import { Storage } from './../../../service/storage';

import * as _ from 'lodash';

@Component({
    selector: 'main.app-content',
    templateUrl: './recipe-full.html'
})

export class RecipeFull {
    mainFeed: any;
    errorFeed: boolean;
    id: string;
    item: any;
    load: boolean;

    constructor(
        private appGlobal: AppGlobal,
        private storage: Storage,
        private feedService: FeedService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.appGlobal.scrollToTop();
        this.mainFeed = this.storage.getData('main-feed', '') || '';
        this.route.params.subscribe(params => this.id = params.id);

        if (!this.mainFeed) {
            this.getMainFeed();
        } else {
            this.load = true;
            this.item = _.find(this.mainFeed, {id: this.id }) || null;
            this.load = false;
        }
    }

    getMainFeed(): any {
        let token = this.storage.getData('token', '');
        let skinList = this.storage.getData('skin-list', '') || '';
        this.errorFeed = false;
        this.load = true;

        this.feedService.mainScreen(token, skinList)
            .then(response => {
                this.mainFeed = response.data.feed_unit_list;
                this.item = _.find(this.mainFeed, {id: this.id }) || null;
                this.load = false;
                this.storage.setDataForTime('main-feed', this.mainFeed, '', 600000);
            })
            .catch(error => {
                this.errorFeed = true;
                this.load = false;
            });
    }

    goBack() {
        let url = this.storage.getData('recipe-url', '') || '';
        if (url !== '') {
          this.storage.removeData('recipe-url', '');
          this.appGlobal.goUrl(url);
        } else {
          this.appGlobal.goBack();
        }
    }

    goNext() {
        this.appGlobal.goUrl('/recipe/' + this.id + '/1');
    }
}
