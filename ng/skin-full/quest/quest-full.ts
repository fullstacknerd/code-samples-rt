import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FeedService } from './../../feed/feed.service';
import { AppGlobal } from './../../../service/global';
import { Storage } from './../../../service/storage';

import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    selector: 'main.app-content',
    templateUrl: './quest-full.html'
})

export class QuestFull {
    mainFeed: any;
    errorFeed: boolean;
    id: string;
    item: any;
    load: boolean;
    timer: any = {
        timer: null,
        all: 0,
        days: '',
        d: '',
        hours: '',
        h: '',
        minutes: '',
        m: ''
    };
    task: any = {
        all: 0,
        done_correct: 0,
        text: ''
    }

    constructor(
        private appGlobal: AppGlobal,
        private storage: Storage,
        private feedService: FeedService,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.appGlobal.scrollToTop();
        this.mainFeed = this.storage.getData('main-feed', '') || '';
        this.route.params.subscribe(params => this.id = params.id);

        if (!this.mainFeed) {
            this.getMainFeed();
        } else {
            this.load = true;
            this.item = _.find(this.mainFeed, {id: this.id }) || null;
            this.calcTime(this.item.countdown_date);
            this.calcTask(this.item.level_list);
            this.load = false;
        }

        console.log(this.item);
    }

    getMainFeed(): any {
        let token = this.storage.getData('token', '');
        let skinList = this.storage.getData('skin-list', '') || '';
        this.errorFeed = false;
        this.load = true;

        this.feedService.mainScreen(token, skinList)
            .then(response => {
                this.mainFeed = response.data.feed_unit_list;
                this.item = _.find(this.mainFeed, {id: this.id }) || null;
                this.calcTime(this.item.countdown_date);
                this.calcTask(this.item.level_list);
                this.load = false;
                this.storage.setDataForTime('main-feed', this.mainFeed, '', 600000);
            })
            .catch(error => {
                this.errorFeed = true;
                this.load = false;
            });
    }

    goBack() {
        let url = this.storage.getData('quest-url', '') || '';
        if (url !== '') {
          this.storage.removeData('quest-url', '');
          this.appGlobal.goUrl(url);
        } else {
          this.appGlobal.goBack();
        }
    }

    calcTime(date: string) {
        let now = moment();
        let finish = moment(date);
        this.timer.all = finish.diff(now, 'minutes');

        this.timer.days = Math.floor(this.timer.all / 1440);
        this.timer.d = this.appGlobal.getWordByNumber(this.timer.days, ['день', 'дня', 'дней']);
        this.timer.hours = Math.floor((this.timer.all - (this.timer.days * 1440))  / 60);
        this.timer.h = this.appGlobal.getWordByNumber(this.timer.hours, ['час', 'часа', 'часов']);
        this.timer.minutes = Math.floor(this.timer.all - (this.timer.days * 1440 + this.timer.hours * 60));
        this.timer.m = this.appGlobal.getWordByNumber(this.timer.minutes, ['минута', 'минуты', 'минут']);

        if (this.timer.all > 0) {
            this.timer.timer = setTimeout(() => {
                this.calcTime(this.item.countdown_date);
            }, 60000);
        } else {
            clearTimeout(this.timer.timer);
        }
    }

    calcTask(level_list: Array<any>) {
        level_list.forEach((level: any) => {
            this.task.all = this.task.all + level.task_list.length;

            level.task_list.forEach((task: any) => {
                if (task.status === 'done_correct') {
                    this.task.done_correct++;
                }
            });
        });

        this.task.text = 'Успешно пройдено ' + this.task.done_correct + ' из ' + this.task.all + this.appGlobal.getWordByNumber(this.task.all, [' задания', ' заданий', ' заданий']);
    }

    goMission(level:number, task: number, status: string) {
        if (status !== 'unavailable') {
          this.appGlobal.goUrl('/quest/' + this.item.id + '/' + level + '/' + task);
        }
    }
}
