/**
 * Конфиг дял сборки проекта на vue (сайт, не SPA)
 */

const path = require('path')
const webpack = require('webpack')
const BabiliPlugin = require('babili-webpack-plugin')
const ProgressBarPlugin = require('progress-bar-webpack-plugin')

const isProd = (process.env.NODE_ENV === 'production')

const app = './dist/'
const src = './assets/'
const paths = {
    build: {
        app: app + 'app'
    },
    assets: {
        app: src + 'app/app.js'
    }
}

const config = {
    entry: {
        main: path.resolve(paths.assets.app),
    },
    output: {
        path: path.resolve(paths.build.app),
        filename: '[name].[chunkhash].js',
        sourceMapFilename: '[file].map'
    },
    devtool: '#source-map',
    module: {
      loaders:[{
        loader: "babel-loader",
        test: /\.js$/,
        exclude: /node_modules/,
        query: {
          plugins: ['transform-runtime'],
          presets: ['es2015', 'stage-0'],
        }
      }]
    },
    plugins: [
        new ProgressBarPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: function (module) {
                // this assumes your vendor imports exist in the node_modules directory
                return module.context && module.context.indexOf('node_modules') !== -1
            }
        }),
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(false),
            RELEASE: JSON.stringify(new Date().getTime()),
            'process.env': {
                NODE_ENV: JSON.stringify(isProd
                    ? 'production'
                    : 'development'
                )
            }
        }),
        new webpack.ProvidePlugin({
           $: "jquery",
           jQuery: "jquery"
        })
    ],
    resolve: {
        modules: [path.resolve(__dirname, 'assets', 'app'), 'node_modules'],
        alias: {
            'vue$': path.resolve(path.join(__dirname, 'node_modules', 'vue', 'dist', 'vue.esm.js'))
        }
    }
}


module.exports = config
