const NODE_ENV = process.env.NODE_ENV || 'production';
const minimize = NODE_ENV !== 'development';

let path = require('path');
let fs = require('fs');
let webpack = require('webpack');
let UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const WORK_DIR = path.resolve(__dirname);
const STATIC_DIR = path.resolve(__dirname, '../');
const JS_DIR = path.join(STATIC_DIR, 'js');



function getJsEntriesList() {
    let files = fs.readdirSync(JS_DIR + '/page'),
        result = {};

    for (let i = 0; i < files.length; ++i) {
        result[files[i]] = ['babel-polyfill', '../js/page/' + files[i]];
    }

    return result;
}

let configuration = {
  entry: getJsEntriesList(),
  output: {
    filename: '[name]',
    sourceMapFilename: '[name].map'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|vendor)/,
        loader: 'babel-loader',
        query: {
          presets: [
            WORK_DIR + '/node_modules/babel-preset-env',
            // ['latest', { modules: false }],
          ],
        },
      },
    ],
  },
  plugins: [],
};

if (minimize) {
    configuration.plugins.push(
      new UglifyJsPlugin({
        sourceMap: true
     })
    );
}

module.exports = configuration;
