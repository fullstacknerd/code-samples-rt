'use strict';
// generated on <%= (new Date).toISOString().split('T')[0] %> using <%= pkg.name %> <%= pkg.version %>


// Plugins
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const del = require('del');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require('./webpack.config.js');

sass.compiler = require('node-sass');

let prefixer_config = ['last 2 versions', 'ie >= 9', 'android >= 4.4', 'ios >= 7'];


const PATH = {
    build: {
        css: '../build',
        js: '../build'
    },
    src: {
        less: '../less/style.less',
        scss: '../scss/style.scss',
        js: '../js/page'
    },
    watch: {
        less: '../less/**/*.less',
        scss: '../scss/**/*.scss',
        js: '../js/**/*.js',
    },
    clean: {
        css: ['../build/*.css', '../build/*.map'],
        js: '../build/*.js'
    }
};


// SCSS
gulp.task('sass:build', ['css:clean'], () => {
    gulp.src( `${PATH.src.scss}` )

      // Base CSS
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest(`${PATH.build.css}`))

      // Minified CSS with vendor prefixes and sourcemaps
      .pipe(rename({suffix: '.min'}))
      .pipe(autoprefixer(prefixer_config))
      .pipe(cleanCSS({compatibility: '*'}, (details) => {
          console.log(`Stats for ${details.name}:
               Original file size: ${details.stats.originalSize}.
               Minified file size: ${details.stats.minifiedSize}.
               Efficiency: ${details.stats.efficiency}. Time spent: ${details.stats.timeSpent}ms.`
          );
      }))
      .pipe(sourcemaps.write('.'))

      .pipe(gulp.dest(`${PATH.build.css}`));
});


// JS
gulp.task('js:build', ['js:clean'], () => {
    gulp.src( `${PATH.src.js}` )
        .pipe(webpackStream(webpackConfig), webpack)
        .pipe(gulp.dest(`${PATH.build.js}`));
});

// Clean CSS
gulp.task('css:clean', () => {
    del(PATH.clean.css, {force: true});
});


// Clean JS
gulp.task('js:clean', () => {
    del([PATH.clean.js], {force: true});
});

gulp.task('watch', () => {
    gulp.watch(`${PATH.watch.scss}`, ['sass:build']);
    // gulp.watch(`${PATH.watch.less}`, ['less:build']);
    gulp.watch(`${PATH.watch.js}`, ['js:build']);
});

gulp.task('build', ['sass:build', 'js:build']);
gulp.task('default', ['build', 'watch']);


// Rev all files
// gulp.task('rev', function () {
//   gulp.src('../dist/**')
//     .pipe(revall({ ignore: [/^\/favicon.ico$/g, '.html'] }))
//     .pipe(gulp.dest('rev'));
// });
